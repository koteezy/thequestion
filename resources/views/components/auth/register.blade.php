<br><br><br><br>
<div class="col-lg-8 col-lg-offset-2">
    <div class="m-b-lg p-h">
        <h5 class="font-normal b-b b-a p-b-xs m-b"> {{ trans('messages.titles.register') }} </h5>
        <form method="post" class="bv-form" action="{{ route('register') }}">

            @include('components.errors')
            @include('components.flashes')

            <div class="form-group has-feedback">
                <label>First name</label>
                <input type="text" class="form-control" name="first_name" value="{{ old('first_name') }}">
            </div>

            <div class="form-group has-feedback">
                <label>Last name</label>
                <input type="text" class="form-control" name="last_name" value="{{ old('last_name') }}">
            </div>

            <div class="form-group has-feedback">
                <label>Email</label>
                <input type="text" class="form-control" name="email" value="{{ old('email') }}">
            </div>

            <div class="form-group has-feedback">
                <label>Password</label>
                <input type="password" class="form-control" name="password">
            </div>

            @include('components.auth.networks_auth')

            {{ csrf_field() }}

            <div class="form-group">
                <button type="submit" class="btn btn-primary">
                    {{ trans('messages.register.sign_up_button') }}
                </button>
            </div>
        </form>
    </div>
</div>
<div class="row">
    <div class="col-md-8 col-md-offset-2">

        <div class="text-center">
            <h4>{{ trans('messages.networks.sign_with') }}</h4>
        </div>

        <div class="row">
            @component('components.networks', [
                    'networks' => $networks,
                    'button' => true,
            ])@endcomponent
        </div>
    </div>
</div>
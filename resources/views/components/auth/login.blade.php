<br><br><br><br>
<div class="col-lg-8 col-lg-offset-2">
    <div class="m-b-lg p-h">
        <h5 class="font-normal b-b b-a p-b-xs m-b"> {{ trans('messages.titles.login') }} </h5>
        <form method="post" class="bv-form" action="{{ route('login') }}">

            @include('components.errors')

            <div class="form-group has-feedback">
                <label>Email</label>
                <input type="text" class="form-control" name="email" value="{{ old('email') }}">
            </div>

            <div class="form-group has-feedback">
                <label>Password</label>
                <input type="password" class="form-control" name="password">
            </div>


           @include('components.auth.networks_auth')

            {{ csrf_field() }}

            <div class="form-group">
                <button type="submit" class="btn btn-primary">
                    {{ trans('messages.login.sign_in_button') }}
                </button>
            </div>
        </form>
    </div>
</div>
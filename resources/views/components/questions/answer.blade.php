<div class="media">
    <div class="media-left">
        <a class="thumb-sm" href="">
            <img class="media-object img-circle" src="{{ uv($answer->user_avatar) }}">
        </a>
    </div>
    <div class="media-body">
        <div>
            <a href="{{ route('user', $answer->user_id) }}" class="media-heading text-dark font-semi-bold">
                {{ $answer->user_first_name . ' ' . $answer->user_last_name }}
            </a>
            <small class="clear m-b-sm">{{ $answer->created_at->diffForHumans() }}</small>
        </div>
        <p>
            {{ $answer->text }}
        </p>

        <small class="clear">
            <e-answer-rating :passed="{{ json_encode($answer) }}"></e-answer-rating>
            {{--<a class="text-muted" href=""> Unlike (3)</a>--}}
        </small>
    </div>
</div>
@if($questions->count())
    @foreach($questions as $question)
        @include('components.questions.single')
    @endforeach
@else
    <div class="alert alert-info text-center">
        {{ trans('messages.no_items') }}
    </div>
@endif



<div class="text-center">
    {{ $questions->links() }}
</div>
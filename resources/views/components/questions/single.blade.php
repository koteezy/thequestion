<div class="col-sm-4">

    <div class="panel">
        <div class="item img-bg" style="height: 160px; background-color: #fff">
            <div class="top wrapper-lg w-full">
                <div class="pull-right m-t-xxs">
                    <e-question-like :passed="{{ json_encode($question) }}" :short="true"></e-question-like>
                    <a href="" class="text-black"><i class="icon-eye"></i> {{ $question->views_count }}</a>
                </div>
                <div class="clear m-b">
                    <a class="pull-left thumb-xxs m-r-sm" href="{{ route('question', $question->id) }}">
                        <img src="{{ uv($question->user_avatar) }}" alt="..." class="img-full img-circle">
                    </a>
                    <div class="clear m-t-xs p-t-2x">
                        <p class="h6">
                            <a href="" class="text-{{ $question->avatar ? 'white' : 'black' }}">
                                {{ $question->user_first_name . ' ' . $question->user_last_name }}
                            </a>
                        </p>
                    </div>
                </div>
            </div>
            <div class="bottom wrapper-lg w-full">
                <h4 class="h4 text-inline">
                    <a class="text-{{ $question->avatar ? 'white' : 'black' }}"
                       href="{{ route('question', $question->id) }}">
                        {{ $question->question }}
                    </a>
                </h4>
                {{--<small class="text-{{ $question->avatar ? 'white' : 'black' }}">{{ $question->created_at->diffForHumans() }}</small>--}}

            </div>

            @if($question->avatar)
                <img class="img-full" src="{{ furl($question->avatar) }}">
            @endif

        </div>
        <div class="wrapper b-b">
            <p class="m-b-none">
                {{ str_limit($question->description, 100) }}

                @component('components.tags_list', [
                    'tags' => $question->tags,
                    'seeAll' => route('question', $question->id),
                ])@endcomponent
            </p>
        </div>
        <div class="wrapper-lg">

            <a href="{{ route('question', $question->id) }}" class="m-r-xl">
                <span>{{ $question->answers_count }} {{ str_plural('Answer', $question->answers_count) }}</span>
            </a>

            @if(u() && $question->user_id === u()->id)
                <div class="pull-right">

                    @if(!$question->was_moderated)
                        <span class="label label-info">Moderating</span>
                    @endif
                    <a href="{{ route('question.delete', $question->id) }}" class="label label-danger">Delete</a>
                    <a href="{{ route('ask', $question->id) }}" class="label label-success">Edit</a>
                </div>
            @endif
        </div>
    </div>

</div>
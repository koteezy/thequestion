@if($errors->count())

    <ul class="alert alert-warning text-center">
        @foreach($errors->all() as $error)
            <li>{{ $error }}</li>
        @endforeach
    </ul>
@endif
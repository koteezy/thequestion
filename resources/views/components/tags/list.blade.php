<br><br><br><br>

<div class="row">
    <div class="col-md-8 col-md-offset-2">
        <div class="row">
            @foreach($tags as $tag)
                @include('components.tags.single')
            @endforeach
        </div>
    </div>
</div>

<div class="text-center">
    {{ $tags->links() }}
</div>
<div class="col-sm-4">
    <div class="panel tag" style="background-color: {{ $tag->color }};">
        <div class="wrapper">
            <div class="text-center">
                <a href="{{ route('tag', $tag->id) }}">
                    <h4 class="text-white">
                        {{ $tag->name }}
                    </h4>
                </a>
                <br>
                <e-question-follow air :passed="{{ json_encode($tag) }}" type="tags"></e-question-follow>
            </div>
        </div>
    </div>
</div>
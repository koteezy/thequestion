<br><br><br><br>
<div class="col-lg-8 col-lg-offset-2">
    <div class="m-b-lg p-h">
        <h5 class="font-normal b-b b-a p-b-xs m-b"> Profile information </h5>

        @component('components.flashes')@endcomponent

        @include('components.errors')

        <form method="post" action="{{ route('me') }}" enctype="multipart/form-data">
            <div class="form-group has-feedback">
                <label>Profile Picture</label>
                <input type="file" class="form-control" name="avatar">
            </div>

            <div class="form-group has-feedback">
                <label>First name</label>
                <input type="text" class="form-control" name="first_name" value="{{ u()->first_name }}">
            </div>

            <div class="form-group has-feedback">
                <label>Last name</label>
                <input type="text" class="form-control" name="last_name" value="{{ u()->last_name }}">
            </div>

            <div class="form-group has-feedback">
                <label>Email</label>
                <input type="text" class="form-control" name="email" value="{{ u()->email }}">
            </div>

            <div class="form-group has-feedback">
                <label>About</label>
                <textarea class="form-control" name="about" cols="3" rows="3">{{ u()->about }}</textarea>
            </div>

            <div class="form-group has-feedback">
                <label>Password</label>
                <input type="password" class="form-control" name="password">
            </div>

            <div class="text-center">
                <h4 class="font-thin">{{ trans('messages.networks.title') }}</h4>

                <div class="row">

                    <div class="col-md-8 col-md-offset-2">
                        <div class="row">
                            @component('components.networks', [
                                'networks' => $networks,
                                'redirect' => url()->current()
                            ])@endcomponent
                        </div>
                    </div>

                </div>

            </div>

            {{ csrf_field() }}
            <div class="form-group">
                <button type="submit" class="btn btn-primary">
                    {{ trans('messages.save_button') }}
                </button>
            </div>
        </form>
    </div>
</div>
<section id="features" class="p-v-xxl img-bg img-white att-fixed"
         style="@if($question->avatar) background-image: url({{ furl($question->avatar) }}) @endif">
    <div class="container m-v-xxl">
        <div class="row text-center text-black">
            <div class="col-sm-8 col-sm-offset-2">

                <br><br>
                <p class="text-2x text-u-c font-semi-bold p-t">
                    {{ $question->question }}
                </p>

                @component('components.tags_list', [
                       'tags' => $question->tags,
                ])@endcomponent
            </div>
        </div>
    </div>
</section>

<div class="col-sm-8 col-sm-offset-2">
    <div class="pull-left">
        <h4 class="font-thin">{{ $question->answers_count }} {{ str_plural('Answer', $question->answers_count) }}</h4>
    </div>
    <div class="pull-right">
        <e-question-like :passed="{{ json_encode($question) }}"></e-question-like>
        <e-question-follow :passed="{{ json_encode($question) }}" type="questions"></e-question-follow>
    </div>
</div>

<div class="col-sm-8 col-sm-offset-2">

    @component('components.flash', ['type' => 'success', 'key' => 'success'])@endcomponent
    @include('components.errors')

    <div class="">

        @foreach($answers as $answer)
            @include('components.questions.answer')
        @endforeach

        @if(u() && $question->user_id !== u()->id && !$question->answered_by_me)
            <div class="m-t-xxl">
                <h3>{{ trans('messages.want_answer_question') }}</h3>
                <form action="{{ route('question.answer', $question->id) }}" method="post">
                    <div class="form-group">
                        <textarea name="text" class="form-control" rows="3">{{ old('text') }}</textarea>
                    </div>
                    {{ csrf_field() }}
                    <div class="form-group">
                        <button type="submit" class="btn btn-primary">
                            {{ trans('messages.questions.answer_the_question_button') }}
                        </button>
                    </div>
                </form>
            </div>
        @endif

    </div>
</div>
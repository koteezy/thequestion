@if(session()->has($key))
    <div class="alert alert-{{ $key }}">
        {{ session()->get($key) }}
    </div>
@endif
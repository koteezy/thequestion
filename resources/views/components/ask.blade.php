<br><br><br><br>
<div class="col-lg-8 col-lg-offset-2">
    <div class="m-b-lg p-h">
        <h5 class="font-normal b-b b-a p-b-xs m-b"> {{ $title }} </h5>

        @component('components.flash', [
            'key' => 'success',
            'type' => 'success'
        ])@endcomponent

        @include('components.errors')

        <form method="post" action="{{ route('ask', $question->id ?? null) }}" enctype="multipart/form-data">
            <div class="form-group has-feedback">
                <label>Question image</label>
                <input type="file" class="form-control" name="avatar">
            </div>

            <div class="form-group has-feedback">
                <label>Question</label>
                <input type="text" class="form-control" name="question"
                       value="{{ $question->question ?? old('question') }}">
            </div>

            <div class="form-group has-feedback">
                <label>Tags</label>
                <input type="text" class="form-control" name="tags"
                       value="{{ $question ? $question->tags->implode('name', ',') : old('tags') }}">
            </div>

            {{ csrf_field() }}
            <div class="form-group">
                <button type="submit" class="btn btn-primary">
                    {{ trans('messages.save_button') }}
                </button>
            </div>
        </form>
    </div>
</div>
@foreach($tags->take(isset($seeAll) ? 3 : $tags->count()) as $tag)

    @if($tag->is_accepted)
        <a href="{{ route('tag', $tag->id) }}" class="label m-r-2x label-primary">
            {{ $tag->name }}
        </a>
    @endif

@endforeach

@if(isset($seeAll) && ($tags->count() - 3) > 0)
    <a href="{{ $seeAll }}" class="label label-success">
        {{ trans('messages.tags.see_all_tags') }}
    </a>
@endif
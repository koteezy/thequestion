<section style="background-color: {{ $tag->color }};">
    <div class="container">
        <div class="row">
            <div class="col-sm-8 col-sm-offset-2 p-v-xxl text-left">
                <br>
                <h1 class="text-white">{{ $tag->name }}</h1>
                <p class="text-white">{{ $tag->questions_count }} questions.</p>
            </div>
        </div>
    </div>
</section>

<div class="col-sm-8 col-sm-offset-2">
    <div class="pull-right">
        {{--<e-question-like :passed="{{ json_encode($question) }}"></e-question-like>--}}
        <e-question-follow air :passed="{{ json_encode($tag) }}" type="tags"></e-question-follow>
    </div>
</div>

<br><br><br>

@include('components.questions.list')
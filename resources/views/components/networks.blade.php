@foreach($networks as $network)
    <div class="col-md-6 col-xs-6 col-lg-6">
        <a no-air
           href="{{ $network->uuid ? '#' : route('oauth', $network->alias) }}{{ isset($redirect) ? '?redirect=' . $redirect : '' }}"
           class="btn btn-block btn-social btn-{{ $network->icon }}">
            <span class="fa fa-{{ $network->icon }}"></span>

            @if(isset($button))
                {{ trans('messages.networks.sign_in_with', [
                    'name' => $network->name
                ]) }}
            @else
                @if($network->uuid)
                    {{ trans('messages.networks.already_attached') }}
                @else
                    {{ trans('messages.networks.attach', [
                        'name' => $network->name
                    ]) }}
                @endif
            @endif
        </a>
    </div>
@endforeach
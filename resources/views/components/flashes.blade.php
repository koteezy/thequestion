@component('components.flash', ['key' => 'success'])@endcomponent
@component('components.flash', ['key' => 'info'])@endcomponent
@component('components.flash', ['key' => 'error'])@endcomponent
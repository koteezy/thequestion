<header class="bg-white navbar-fixed-top box-shadow">
    <div class="container">
        <div class="navbar-header">
            <button class="btn btn-link visible-xs pull-right m-r m-t-sm" type="button" data-toggle="collapse"
                    data-target=".navbar-demo-4">
                <i class="fa fa-bars"></i>
            </button>
            <a  @click.prevent="test(1,2,3)" href="{{ url('/') }}" class="navbar-brand m-r-sm">
                <span class="h4 font-bold">TheQuestion</span>
            </a>
        </div>
        <div class="collapse navbar-collapse navbar-demo-4">

            <!-- search form -->
        {{--<form class="navbar-form navbar-left m-v-sm">--}}
        {{--<div class="form-group">--}}
        {{--<div class="input-group">--}}
        {{--<input type="text" class="form-control input-sm bg-light" placeholder="Search...">--}}
        {{--<span class="input-group-btn">--}}
        {{--<button type="submit" class="btn btn-sm bg-light">--}}
        {{--<i class="fa fa-search"></i>--}}
        {{--</button>--}}
        {{--</span>--}}
        {{--</div>--}}
        {{--</div>--}}
        {{--</form>--}}
        <!-- / search form -->

            <ul class="nav navbar-nav font-bold">
                <li>
                    <a q-link href="{{ route('index') }}">Recent</a>
                </li>
                <li>
                    <a q-link href="{{ route('index', 'top') }}">Top</a>
                </li>
                @if(u())
                    <li>
                        <a q-link href="{{ route('feed') }}">My Feed</a>
                    </li>
                @endif
                <li>
                    <a q-link href="{{ route('tags') }}">Tags</a>
                </li>
                @if(u())
                    <li>
                        <a q-link href="{{ route('ask') }}">Ask</a>
                    </li>
                @endif
            </ul>


            @if(auth()->check())

                <ul class="nav navbar-nav navbar-right">
                    <li class="dropdown">
                        <a href="" class="dropdown-toggle clear" data-toggle="dropdown">
								<span class="thumb-sm avatar pull-right m-t-n-sm m-b-n-sm m-l-sm">
									<img src="{{ uv(u()->avatar) }}" alt="...">
									<i class="on md b-white bottom"></i>
								</span> <span class="hidden-sm hidden-md">
                                {{ u()->fullName }}
                            </span>
                        </a>
                        <!--dropdown -->
                        <ul class="dropdown-menu w">
                            <li>
                                <a href="{{ route('my_questions') }}">
                                    <span>My questions</span>
                                </a>
                            </li>
                            <li>
                                <a href="{{ route('me') }}">
                                    <span>Settings</span>
                                </a>
                            </li>
                            <li>
                                <a no-air href="{{ route('logout') }}">Logout</a>
                            </li>
                        </ul>
                        <!--/ dropdown -->
                    </li>
                </ul>

            @else
                <ul class="nav navbar-nav font-bold navbar-right">
                    <li>
                        <a href="{{ route('login') }}">Sign in</a>
                    </li>
                    <li>
                        <a href="{{ route('register') }}">Sign Up</a>
                    </li>
                </ul>
            @endif

        </div>
    </div>
</header>

{{--<section class="">--}}
{{--<div class="container">--}}
{{--<div class="row">--}}
{{--<div class="col-sm-8 col-sm-offset-2 p-v-xxl text-center">--}}

{{--</div>--}}
{{--</div>--}}
{{--</div>--}}
{{--</section>--}}
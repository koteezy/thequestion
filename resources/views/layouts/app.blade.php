<!DOCTYPE html>
<html class="no-js">
<head>
    <meta charset="utf-8">
    <title>@yield('title') {{ s()->site_title_separator . ' ' . s()->site_title }}</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width">
    <link rel="shortcut icon" href="/assets/img/favicon.ico">

    <link rel="stylesheet" href="/bower_components/bootstrap/dist/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="/bower_components/font-awesome/css/font-awesome.min.css"/>
    <link rel="stylesheet" href="/bower_components/simple-line-icons/css/simple-line-icons.css"/>
    <link rel="stylesheet" href="/bower_components/fontface-source-sans-pro/css/source-sans-pro.min.css"/>
    <link rel="stylesheet" href="/dist/css/bootaide.min.css"/>

    @yield('head')

    <script>
        window.app = {
            messages: {!! json_encode(\Illuminate\Support\Facades\Lang::get('messages')) !!},
            user: {!! json_encode(auth()->user()) !!},
        };
    </script>

    <style>
        .liked {
            color: red;
        }

        .panel {
            height: 270px;
        }

        .panel.tag {
            height: 135px;
        }
    </style>
</head>
<body>

<div id="app">
    @include('layouts.header')

    <div id="content">
        @yield('main')
    </div>

    <notifications air group="app" animation-type="velocity"/>
</div>

<script src="/bower_components/jquery/dist/jquery.min.js"></script>
<script src="/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<script src="/assets/js/bundle.js"></script>
@yield('scripts')


</body>
</html>

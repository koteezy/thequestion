@extends('layouts.app')


@section('title') Edit profile information @endsection

@section('main')
    @include('components.me.settings')
@endsection

@section('head')
    @include('components.header.css')
@endsection
@extends('layouts.app')

@section('title'){{ $title }}@endsection

@section('main')

    @include('components.feed')

@endsection
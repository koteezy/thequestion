@extends('layouts.app')

@section('title'){{ $title }}@endsection

@section('main')
    @include('components.question')
@endsection
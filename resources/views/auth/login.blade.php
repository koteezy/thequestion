@extends('layouts.app')

@section('title'){{ $title }}@endsection

@section('main')
    @include('components.auth.login')
@endsection

@section('head')
    @include('components.header.css')
@endsection
@extends('layouts.app')

@section('title'){{ $title }}@endsection

@section('main')
    @include('components.auth.register')
@endsection

@section('head')
    @include('components.header.css')
@endsection
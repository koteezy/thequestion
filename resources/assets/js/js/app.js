import { app, changePage } from './ajaxify';
import Vue from 'vue';
import ComponentPlugins from './plugins/components';
import Directives from './directives';
import axios from 'axios';

Vue.use( ComponentPlugins );
Vue.use( Directives );

axios.defaults.headers.common[ 'X-Requested-With' ] = 'XMLHttpRequest';

// start app.
app();


// add needed listeners.
window.addEventListener( 'click', e => {
    e.preventDefault();
    // e     = e || window.event;
    let a      = e.target || e.srcElement,
        parent = a.parentElement,
        child  = a.childNodes[ 0 ];

    if ( a.tagName !== 'A' && parent.tagName === 'A' )
        a = parent;

    if ( a.tagName !== 'A' || !a.href || a.href.slice( -1 ) === '#' ) {
        return;
    }

    changePage( a.href, true );
}, false );//<-- we'll get to the false in a minute

// load content from previous page
window.onpopstate = function ( event ) {
    changePage( document.location, false );
};

let a = {
    uuid: 'jjdkw-jwqk-jmvw-kmwd',
    file: {
        server: 's3.posts',
    },
};
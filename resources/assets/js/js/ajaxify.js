import Vue from 'vue';
import axios from 'axios/index';

let f        = 0;
window.pages = {};

/**
 * Vue base instance.
 *
 * @param handlers
 */
export const app = ( handlers = false ) => {
    setSettings();

    if ( f === 1 ) {
        reRenderElements();
        window.App.$destroy();
    }

    f      = 1;
    let el = document.getElementById( 'app' );

    if ( handlers )
        setHandlers();

    // setHandlers();
    // else {
    // }

    window.App = new Vue( {
        el,
        data: {
            title: 'Fuck u!',
        },
        methods: {

            click() {
                console.log( 'Works.' );
            },

        },
    } );
};

export const replacePageWrapper = ( html ) => {
    var div = document.getElementById( 'page_wrapper' );

    div.innerHTML = html;

    setSettings();

    app( true );
};

export const appendHtml = ( el, str, replace = false ) => {

    setSettings();

    let div = document.createElement( 'div' ),
        y   = window.scrollY,
        x   = window.scrollX;

    div.innerHTML = str;

    if ( replace ) {
        el.innerHTML = str;
    } else {
        while ( div.children.length > 0 ) {
            el.appendChild( div.children[ 0 ] );
        }
    }

    window.scrollTo( x, y );

    setHandlers();

    app( true );
};

export const setHandlers = () => {
    let nodes = document.querySelectorAll( '[settings]' );

    Array.prototype.forEach.call( nodes, function ( el ) {
        let settings = el.getAttribute( 'settings' ),
            json     = JSON.parse( settings );

        if ( json.events ) {
            Object.keys( json.events ).forEach( key => {
                el.setAttribute( 'v-on:' + key + '.prevent', json.events[ key ] );
            } );
        }
    } );
};

export const setSettings = () => {
    let nodes = document.querySelectorAll( '[v-on\\\:click\\\.prevent]' );

    Array.prototype.forEach.call( nodes, el => {
        let params = {
            events: {
                click: el.getAttribute( 'v-on:click.prevent' ),
            },
        };

        el.setAttribute( 'settings', JSON.stringify( params ) );
    } );
};

export const reRenderElements = () => {
    let nodes = document.querySelectorAll( '[air-re-render]' );

    window.test = [];

    Array.prototype.forEach.call( nodes, el => {

        let elName     = el.getAttribute( 'air-re-render' ),
            params     = JSON.parse( el.getAttribute( 'air-params' ) ),
            parameters = '';

        Object.keys( params ).forEach( key => {

            let str = `${key}=`,
                val = params[ key ];

            if ( typeof val === 'object' || Array.isArray( val ) ) {
                str = `:${str}`;
                str += `'${JSON.stringify( val )}'`;
            }
            else
                str += `"${val}"`;

            parameters += ` ${str}`;
        } );

        el.outerHTML = `<${elName} ${parameters}></${elName}>`;
    } );
};


export const changePage = ( url, replaceState ) => {

    let hash = window.pages[ url ];

    if ( hash ) {
        // replace contnet.
        replacePageWrapper( hash.html );

        // change state.
        if ( replaceState ) {
            history.pushState( {title: hash.title, url}, hash.title, url );
        }

        return;
    }

    return axios.get( url )
                .then( ( {data} ) => {

                    let title = data.content.title;

                    // change title.
                    document.title = title;

                    // change state.
                    if ( replaceState ) {
                        history.pushState( {title, url: url}, title, url );
                    }

                    // replace contnet.
                    replacePageWrapper( data.content.html );

                    window.scrollTo( 0, 0 );


                    // console.log( document.getElementById( 'page_wrapper' ).outerHTML );
                    window.pages[ url ] = {
                        title,
                        html: document.getElementById( 'page_wrapper' ).outerHTML,
                    };
                } )
                .catch( err => {
                    console.log( err );
                    console.log( 'Error was occuped.' );
                } );
};
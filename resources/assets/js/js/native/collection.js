import axios from 'axios';
import { Model } from './model';

class Collection {
    constructor( model = null, additionalFields = {} ) {

        Collection.instances.push( this );

        this.url              = null;
        this.parameters       = {};
        this.page             = 1;
        this.lastPage         = 1;
        this.loading          = false;
        this.loadingMore      = false;
        this.items            = [];
        this.model            = model; // all items will be that model.
        this.wasLoaded        = false; // set true after first loading.
        this.hasNewItems      = false;
        this.previousUrlHash  = null;
        this.additionalFields = additionalFields;

        this.freshLoad = ( url, parameters ) => {
            this.page     = 0;
            this.lastPage = 0;

            return this.load( url, parameters );
        };

        this.loadMore = () => {
            this.loadingMore = true;
            return this.load( this.url, this.parameters );
        };

        this.hasMorePages = () => {
            return this.lastPage > this.page;
        };

        this.setItems = data => {
            let items = data.data ? data.data : data;

            // its a paginator.
            if ( this.page > 1 ) {
                this.items = this.items.concat( this.getModelItems( items ) );
            } else {
                this.items = this.getModelItems( items );
            }

            // if it a laravel paginator.
            if ( !Array.isArray( data ) ) {
                this.page     = data.current_page;
                this.lastPage = data.last_page;
            }

            this.loading     = false;
            this.loadingMore = false;
            this.hasNewItems = true;
        };

        this.getModelItems = items => {
            let additionalFields = this.additionalFields;
            return items.map( item => {

                if ( additionalFields ) {
                    Object.keys( additionalFields ).forEach( key => {
                        item[ key ] = additionalFields[ key ];
                    } );
                }

                return this.model ? new this.model( item ) : item;
            } );
        };

    }

    freshParameters() {
        let o = {};

        o.params = {...this.parameters, page: this.page};

        this.wasLoaded = false;

        return o;
    };

    load( url, parameters = {} ) {

        this.url         = url;
        this.parameters  = parameters;
        this.hasNewItems = false;

        return new Promise( ( resolve, reject ) => {

            if ( this.loadingMore ) {
                this.page++;
            } else {
                this.loading = true;
            }

            // if we trying load previous loaded url
            if ( this.isPreviousUrlHash() ) {
                return resolve( this );
            }

            this.setPreviousUrlHash();

            axios.get( this.url, this.freshParameters() ).then( response => {
                this.setItems( response.data );
                this.wasLoaded   = true;
                this.hasNewItems = true;

                resolve( this.items );
            } ).catch( err => {
                console.log( err );
                reject( err.response );
            } );
        } );
    };

    getPreviousUrlHash() {
        return JSON.stringify( {
            url: this.url,
            params: this.freshParameters(),
        } );
    }

    setPreviousUrlHash() {
        this.previousUrlHash = this.getPreviousUrlHash();
    }

    isPreviousUrlHash() {
        return this.previousUrlHash === this.getPreviousUrlHash();
    }
}

Collection.instances = [];

export { Collection };
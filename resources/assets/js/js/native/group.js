import { Model } from './model';
import axios from 'axios';

export default class Group extends Model {

    constructor( defaultFields ) {
        super( defaultFields );

        this.followButtonWasClicked = false;
    }

    follow() {

        if ( this.followButtonWasClicked )
            return;

        this.followButtonWasClicked = true;

        axios.post( `/api/groups/${this.id}/follow` )
             .then( () => {
                 this.is_followed            = !this.is_followed;
                 this.followButtonWasClicked = false;
             } );
    }
}
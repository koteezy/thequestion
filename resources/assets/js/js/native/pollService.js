import Vue from 'vue';
import axios from 'axios';

let interval = null,
    polls    = [],
    results  = {},
    load     = false;

export class PollService {

    static poll( poll ) {

        // if already in polls.
        if ( polls.map( p => p.hash ).indexOf( poll.hash ) > -1 )
            return;

        // if()

        polls.push( poll );

        if ( interval )
            clearInterval( interval );

        interval = setTimeout( () => {
            this.fetchPolls();
        }, 300 );
    };

    static fetchPolls() {
        if ( !polls.length )
            return;

        axios.get( '/api/polls', {
            params: {
                hashes: polls.map( poll => poll.hash ),
            },
        } ).then( ( {data} ) => {

            Object.keys( data ).forEach( key => {
                results[ key ] = data[ key ];
            } );

            polls.forEach( poll => {
                let loadedPoll = data[ poll.hash ];

                if ( !loadedPoll ) {
                    loadedPoll = {items: {}};

                    Object.keys( poll.items ).forEach( key => loadedPoll.items[ key ] = 0 );
                }

                Object.keys( loadedPoll.items ).forEach( ( key ) => {
                    Vue.set( poll.items[ key ], 'count', loadedPoll.items[ key ] );
                } );

                Vue.set( poll, 'wasLoaded', true );
                Vue.set( poll, 'total', loadedPoll.total || 0 );
                Vue.set( poll, 'voted', loadedPoll.voted || null );
            } );

            polls   = [];
            results = {};
        } );
    }

}
import Vue from 'vue';
import axios from 'axios';

export const modelObject = {

    previousUrl: 1,

    load: ( url, parameters ) => {
        return new Promise( ( resolve, reject ) => {

            if ( url === modelObject.previousUrl )
                return resolve();

            console.log( url );
            axios.get( url, parameters )
                 .then( response => {
                     modelObject.previousUrl = url;
                     Object.assign( this, response.data );
                     resolve( this );
                 } )
                 .catch( err => {
                     reject( err.response );
                 } );

        } );
    },

    /**
     * Set fields to that model.
     *
     * @param object
     */
    setFields: object => {
        Object.keys( object ).forEach( key => {
            Vue.set( key, object[ key ] );
        } );
    },

};
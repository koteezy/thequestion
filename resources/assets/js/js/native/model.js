import Vue from 'vue';
import axios from 'axios';

export class Model {

    constructor( defaultFields = {} ) {

        this.previousUrl   = null;
        this.defaultFields = defaultFields;

        this.load = ( url, parameters ) => {
            return new Promise( ( resolve, reject ) => {

                if ( url === this.previousUrl )
                    return resolve( this );

                axios.get( url, parameters )
                     .then( response => {
                         this.previousUrl = url;
                         this.setFields( response.data );
                         resolve( this );
                     } )
                     .catch( err => {
                         reject( err.response );
                     } );

            } );
        };

        /**
         * Set fields to that model.
         *
         * @param object
         */
        this.setFields = object => {

            for ( let key in object ) {
                this[ key ] = object[ key ];
            }
            // Object.keys( object ).forEach( key => {
            //     Vue.set( this, key, object[ key ] );
            // } );
        };

        this.setFields( this.defaultFields );
    }

}
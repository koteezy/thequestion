import { Collection } from './collection';
import { fetchPolls } from '../helpers/classes';
import Post from './post';

export class PostCollection extends Collection {

    constructor() {
        super( Post );
    }

    load( ...params ) {
        let promise = super.load( ...params );

        promise.then( () => {
            if ( this.hasNewItems )
                fetchPolls( this.items );
        } );

        return promise;
    };

}
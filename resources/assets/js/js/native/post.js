import { Model } from './model';
import axios from 'axios';

export default class Post extends Model {

    constructor( defaultFields ) {
        super( defaultFields );

        this.ratingWasClicked   = false;
        this.favoriteWasClicked = false;
    }

    someMethod() {
        // console.log( super );
        this.title = 123;
    }

    like() {
        this.likeOrDislike( 1 );
    }

    dislike() {
        this.likeOrDislike( -1 );
    }

    favorite() {

        if ( this.favoriteWasClicked )
            return;

        this.favoriteWasClicked = true;

        if ( this.is_favorited )
            this.favorites_count--;
        else
            this.favorites_count++;

        this.is_favorited = !this.is_favorited;

        axios.post( `/api/favorite/posts/${this.id}`, {
            delete: !this.is_favorited,
        } ).then( () => {
            this.favoriteWasClicked = false;
        } );
    }


    /**
     * @todo Включить голову будучи не сонным.
     * @param sign
     */
    likeOrDislike( sign ) {
        this.ratingWasClicked = true;

        switch ( sign ) {

            case 1:

                if ( this.rated_as === null )
                    this.rating++;
                else if ( this.rated_as === -1 ) {
                    this.rating++;
                    this.rating++;
                }
                else {
                    this.rating--;
                    sign = null;
                }
                break;

            case -1:

                if ( this.rated_as === null ) {
                    this.rating--;
                }
                else if ( this.rated_as === 1 ) {
                    this.rating--;
                    this.rating--;
                }
                else {
                    this.rating++;
                    sign = null;
                }
                break;
        }


        this.rated_as = sign;

        axios.post( `/api/posts/${this.id}/vote`, {
            sign: sign === null ? undefined : sign,
        } ).then( () => {
            this.ratingWasClicked = false;
        } );
    }
}
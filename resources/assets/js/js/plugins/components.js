export default Vue => {
    Vue.component( 'vSubscribeBtn', require( '../components/buttons/subscribe' ) );
    Vue.component( 'vLikeBtn', require( '../components/buttons/like' ) );
    Vue.component( 'vLoadMoreBtn', require( '../components/buttons/load_more' ) );

    Vue.component( 'VPostPoll', require( '../components/posts/blocks/poll' ) );
}
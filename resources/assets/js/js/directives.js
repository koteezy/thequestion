import axios from 'axios';
import { appendHtml } from './ajaxify';

window.nodes = [];

export default Vue => {

    Vue.directive( 'loadMore', {
        inserted: el => {
            el.onclick = () => {
                let url     = el.getAttribute( 'url' ),
                    loading = el.getAttribute( 'loading' );

                if ( loading === 1 )
                    return;

                console.log( 'clicked.' );

                el.setAttribute( 'loading', 1 );

                axios.get( url ).then( ( {data} ) => {
                    el.remove();

                    appendHtml( document.getElementById( 'feed_container' ), data );
                } );
            };
        },
    } );
}
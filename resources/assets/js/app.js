import Vue from 'vue';

import './plugins';
import './components';
import './libraries';
import Air from './classes/air';

const app = el => {

    // Initialize settings before start vue.
    Air.initAttributes();

    new Vue( {

        data() {
            return {
                user: window.app.user,
            };
        },

    } ).$mount( el );
};

new Air( app );

app( '#app' );

// load content from previous page
window.onpopstate = function ( event ) {
    event.preventDefault();

    const url = document.location.href;

    Air.loadContent( url ).then( data => {
        Air.updateByJson( data );
    } );
};

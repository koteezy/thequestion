import Vue from 'vue';

Vue.component( 'eAnswerRating', require( './components/answers/rating' ) );
Vue.component( 'eQuestionLike', require( './components/question/like' ) );
Vue.component( 'eQuestionFollow', require( './components/question/follow' ) );
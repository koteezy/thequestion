export default ( Vue ) => {

    Vue.mixin( {

        computed: {

            isAuth() {
                let user = this.user || this.$root.user;

                return user && user.id;
            },

        },

        methods: {

            actionRestrict() {
                this.$notify( {
                    group: 'app',
                    type: 'warn',
                    title: 'Error',
                    text: 'Only authorized users can perform this action.',
                } );
            },

        },

    } );

}
import axios from 'axios';

axios.defaults.headers[ 'X-Requested-With' ] = 'XMLHttpRequest';

let vue = null;

export default class Air {

    constructor( vueInstance ) {
        vue = vueInstance;
    }

    /**
     * Store attribute name.
     *
     * @return {string}
     */
    static get attributesKey() {
        return 'air-attributes';
    }

    /**
     *
     * @return {string}
     */
    static get contentKey() {
        return 'content';
    }

    /**
     * Copy all attributes into one attribute, in the form of JSON,
     * in order to save them when the application is reinitialized.
     */
    static initAttributes() {

        this.setLinksListeners();

        // let nodes = document.querySelectorAll( '[v-on\\\:click\\\.prevent]' );
        let nodes  = document.querySelectorAll( '[air]' ),
            except = [ 'air', this.attributesKey, 'class', 'href' ];

        Array.prototype.forEach.call( nodes, el => {
            let attributes = {},
                tagName    = el.tagName.toLowerCase();

            Array.prototype.forEach.call( el.attributes, attribute => {
                let name     = attribute.nodeName,
                    replaced = name.replace( '@', 'v-on:' ); // we can`t set attribute like @name="method"

                if ( except.indexOf( name ) === -1 ) {
                    attributes[ replaced ] = el.getAttribute( name );
                }

            } );


            // is a vue element?
            if ( this.isVueElement( tagName ) ) {
                el.setAttribute( 'air-element', tagName );
            }

            el.setAttribute( this.attributesKey, JSON.stringify( attributes ) );
        } );
    }

    /**
     * Put the saved listeners, and other methods.
     */
    static setAttributes() {

        let nodes = document.querySelectorAll( `[${this.attributesKey}]` );

        Array.prototype.forEach.call( nodes, node => {
            let attributes  = JSON.parse( node.getAttribute( this.attributesKey ) ),
                elementName = node.getAttribute( 'air-element' );

            Object.keys( attributes ).forEach( key => {
                node.setAttribute( key, attributes[ key ] );
            } );


            // need a re-render vue element?
            if ( elementName ) {

                let tagAttributes = Array.prototype.map.call( node.attributes, el => {
                    let name  = el.nodeName,
                        value = `${node.getAttribute( name )}`;

                    return `${name}='${value}'`;
                } ).join( ' ' );

                node.outerHTML = `<${elementName} ${tagAttributes}></${elementName}>`;

                console.log( 'Reinit node - ' + elementName );
            }
        } );
    }

    /**
     * Set listeners for
     */
    static setLinksListeners() {
        window.onclick = el => {

            let a = el.target.tagName === 'A' ? el.target : el.target.parentNode;

            if (
                a.tagName !== 'A'
                || a.href.replace( '#', '' ) === window.location.href.replace( '#', '' )
                || !a.href
                || a.hasAttribute( 'no-air' ) )
                return;

            el.preventDefault();

            this.loadContent( a.href ).then( data => {
                this.updateByJson( data, a.href );
            } );
        };
    }

    static loadContent( url ) {
        return new Promise( ( resolve, reject ) => {
            axios.get( url, {
                params: {
                    mode: 'html',
                },
            } ).then( ( {data} ) => {
                resolve( data );
            } );
        } );
    }

    /**
     * Update current url, and title.
     *
     * @param url
     * @param title
     */
    static updateUrl( title, url = null ) {
        document.title = title;

        if ( url )
            history.pushState( url, title, url );
    }


    /**
     * Update dom content.
     *
     * @param html
     */
    static updateContent( html ) {
        // this.initAttributes();

        document.getElementById( this.contentKey ).innerHTML = html;

        this.setAttributes();

        vue( '#app' );

        window.scrollTo( 0, 0 );
    }

    /**
     * Update data by data from server.
     *
     * @param data
     * @param url
     */
    static updateByJson( data, url ) {
        this.updateUrl( data.content.title, url );
        this.updateContent( data.content.html );
        this.updateStylesheets( data.stylesheets || [] );
    }

    /**
     * Check is a vue custom component?
     *
     * @param tagName
     * @return {boolean}
     */
    static isVueElement( tagName ) {
        let elements = [ 'div', 'a', 'p', 'h1', 'h2', 'h3' ];

        return elements.indexOf( tagName ) === -1;
    }

    /**
     * Add css
     *
     * @param stylesheets
     */
    static updateStylesheets( stylesheets ) {
        let head = document.getElementsByTagName( 'head' )[ 0 ];

        stylesheets.forEach( url => {

            // if we already append that, we do not need make this again
            if ( document.querySelector( `[href="${url}"]` ) )
                return;

            let style  = document.createElement( 'link' );
            style.href = url;
            style.type = 'text/css';
            style.rel  = 'stylesheet';
            head.append( style );
        } );

    }
}
<?php

return [
    'titles' => [
        'popular_feed'    => 'Popular questions',
        'feed'            => 'Latest questions',
        'my_feed'         => 'My feed',
        'tags'            => 'All tags',
        'create_question' => 'Create new question',
        'edit_question'   => 'Edit question',
        'login'           => 'Sign in',
        'register'        => 'Sign up',
        'edit_profile'    => 'Edit profile information',
    ],

    'register' => [
        'sign_up_button' => 'Sign up',
    ],
    'login'    => [
        'sign_in_button' => 'Sign in',
    ],

    'tags'      => [
        'follow'       => 'Follow on the tag',
        'unfollow'     => 'Unfollow from the tag',
        'see_all_tags' => "See all tags",
    ],
    'questions' => [
        'follow'                     => 'Follow on the question',
        'unfollow'                   => 'Unfollow from the question',
        'answer_created'             => 'Answer was created!',
        'success_delete'             => 'Question was successfully deleted',
        'success_update'             => 'Question was successfully updated',
        'success_create'             => 'Question was created',
        'success_create_moderate'    => 'The question will appear in the feed after the moderator checks it.',
        'answer_the_question_button' => "Answer the question",
    ],
    'user'      => [
        'profile_updated' => 'Profile successful updated!',
    ],
    'networks'  => [
        'title'                                  => 'Social networks',
        'already_attached'                       => 'Already attached',
        'attach'                                 => 'Attach :name',
        'sign_with'                              => 'Sign up with social networks',
        'sign_in_with'                           => 'Sign in with :name',
        'continue_register'                      => 'Your social profile information is not enough. Please fill in all fields.',
        'already_attached_to_another_account'    => 'It seems that such a social profile is already attached to another account.',
        'network_already_attached_to_my_account' => 'It seems that such a social profile is already attached to your account.',
    ],

    'follower'             => 'Follower',
    'no_items'             => 'Looks like we have nothing to show you yet...',
    'want_answer_question' => 'Do you want to answer the question?',
    'save_button'          => 'Save',
];
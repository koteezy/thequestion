<?php

use Faker\Generator as Faker;

$factory->define( App\Models\Question\QuestionAnswer::class, function ( Faker $faker ) {
    return [
        'user_id' => \App\User::inRandomOrder()->first()->id ?? factory( \App\User::class )->create()->id,
        'text'    => $faker->realText( random_int( 200, 700 ) ),
    ];
} );

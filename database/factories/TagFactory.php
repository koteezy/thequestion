<?php

use Faker\Generator as Faker;

$factory->define( \App\Models\Tag\Tag::class, function ( Faker $faker ) {

    $colors = [
        '#cf84c2',
        '#e1865c',
        '#ee7c75',
        '#59ad8a',
        '#64a8ab',
        '#e2837d',
        '#4eb14f',
        '#bf86e5',
        '#3aacb3',
        '#e6807f',
        '#34b272',
        '#8fa47b',
        '#b594a8',
        '#c28eae',
        '#00b3ae',
        '#63a1e6',
    ];

    return [
        'name'        => $faker->streetName,
        'description' => $faker->realText( 200 ),
        'color'       => $colors[ array_rand( $colors ) ],
        'is_accepted' => true,
    ];
} );

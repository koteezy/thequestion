<?php

use Faker\Generator as Faker;

$factory->define( App\Models\Question\Question::class, function ( Faker $faker ) {
    $needImage = random_int( 1, 15 ) > 14;
    $avatar    = null;

    if ( $needImage ) {
        $avatar = uploadRandomImage( 'https://source.unsplash.com/random/1280x720' );
    }

    return [
        'user_id'       => \App\User::inRandomOrder()->first()->id ?? factory( \App\User::class )->create()->id,
        'question'      => substr_replace( $faker->realText( 80 ), '?', -1, 1 ),
        'was_moderated' => true,
        'avatar'        => $avatar,
    ];
} );

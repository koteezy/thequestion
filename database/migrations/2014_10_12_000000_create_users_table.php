<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up ()
    {
        Schema::create( 'users', function ( Blueprint $table ) {
            $table->increments( 'id' );
            $table->string( 'first_name' );
            $table->string( 'last_name' );
            $table->string( 'email' )->unique();
            $table->string( 'password' )->nullable();
            $table->string( 'avatar' )->nullable();
            $table->string( 'about' )->nullable();
            $table->boolean( 'notify_when_new_answer' )->default( true );
            $table->boolean( 'notify_when_answer_deleted' )->default( true );
            $table->boolean( 'notify_when_answer_accepted' )->default( true );
            $table->boolean( 'notify_when_answer_new_comment' )->default( true );
            $table->boolean( 'notify_when_question_deleted' )->default( true );
            $table->rememberToken();
            $table->timestamps();
        } );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down ()
    {
        Schema::dropIfExists( 'users' );
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQuestionViewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up ()
    {
        Schema::create( 'question_views', function ( Blueprint $table ) {
            $table->integer( 'question_id' )->unsigned();
            $table->integer( 'user_id' )->nullable();
            $table->string( 'hash' );
            $table->ipAddress( 'ip' );
            $table->timestamps();
        } );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down ()
    {
        Schema::dropIfExists( 'question_views' );
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserNetworksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up ()
    {
        Schema::create( 'user_networks', function ( Blueprint $table ) {
            $table->integer( 'network_id' )->unsigned();
            $table->integer( 'user_id' )->unsigned();
            $table->string( 'uuid' );
        } );

        Schema::table( 'user_networks', function ( Blueprint $table ) {
            $table->foreign( 'network_id' )->references( 'id' )->on( 'networks' )->onDelete( 'cascade' );
            $table->foreign( 'user_id' )->references( 'id' )->on( 'users' )->onDelete( 'cascade' );
        } );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down ()
    {
        Schema::dropIfExists( 'user_networks' );
    }
}

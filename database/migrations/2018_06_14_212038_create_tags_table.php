<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTagsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up ()
    {
        Schema::create( 'tags', function ( Blueprint $table ) {
            $table->increments( 'id' );
            $table->string( 'name', 50 );
            $table->string( 'description' )->nullable();
            $table->string( 'color', 15 )->default( '#cf84c2' );
            $table->boolean( 'is_accepted' )->default( false );
            $table->timestamps();
        } );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down ()
    {
        Schema::dropIfExists( 'tags' );
    }
}

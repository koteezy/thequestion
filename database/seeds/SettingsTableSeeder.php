<?php

use Illuminate\Database\Seeder;

class SettingsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run ()
    {
        \Illuminate\Support\Facades\DB::table( 'settings' )->insert( [
            [
                'site_title'           => 'theQuestions',
                'site_title_separator' => '~',
                'moderate_questions'   => true,
            ],
        ] );
    }
}

<?php

use Illuminate\Database\Seeder;

class TagsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run ()
    {
        $tags = [
            [
                'name'        => 'Psychology',
                'color'       => '#cf84c2',
                'is_accepted' => true,
            ],
            [
                'name'        => 'Education',
                'color'       => '#e1865c',
                'is_accepted' => true,
            ],
            [
                'name'        => 'Cinema',
                'color'       => '#59ad8a',
                'is_accepted' => true,
            ],
            [
                'name'        => 'Internet',
                'color'       => '#64a8ab',
                'is_accepted' => true,
            ],
            [
                'name'        => 'Technologies',
                'color'       => '#bf86e5',
                'is_accepted' => true,
            ],
            [
                'name'        => 'Music',
                'color'       => '#4eb14f',
                'is_accepted' => true,
            ],
        ];

        \Illuminate\Support\Facades\DB::table('tags')->insert($tags);
    }
}

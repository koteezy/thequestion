<?php

use Illuminate\Database\Seeder;

class AdminMenusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run ()
    {
        \Illuminate\Support\Facades\DB::table( 'admin_menu' )->insert( [
            [
                'order'     => 8,
                'parent_id' => 0,
                'title'     => 'Question',
                'icon'      => 'fa fa-question',
                'uri'       => 'questions',
            ],
            [
                'order'     => 9,
                'parent_id' => 0,
                'title'     => 'Tags',
                'icon'      => 'fa fa-hashtag',
                'uri'       => 'tags',
            ],
            [
                'order'     => 10,
                'parent_id' => 0,
                'title'     => 'Answers',
                'icon'      => 'fa fa-check',
                'uri'       => 'answers',
            ],
            [
                'order'     => 11,
                'parent_id' => 0,
                'title'     => 'Users',
                'icon'      => 'fa fa-users',
                'uri'       => 'users',
            ],
            [
                'order'     => 12,
                'parent_id' => 0,
                'title'     => 'Settings',
                'icon'      => 'fa fa-cog',
                'uri'       => 'settings/1/edit',
            ],
            [
                'order'     => 13,
                'parent_id' => 0,
                'title'     => 'Networks',
                'icon'      => 'fa fa-globe',
                'uri'       => 'networks',
            ],
        ] );
    }
}

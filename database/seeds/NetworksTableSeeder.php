<?php

use Illuminate\Database\Seeder;

class NetworksTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run ()
    {
        $networks = [
            [
                'name'          => 'Vkontakte',
                'alias'         => 'vkontakte',
                'client_id'     => 6254347,
                'client_secret' => 'SGjqd1xOddnocZoo6VSg',
                'icon'          => 'vk',
                'is_active'     => true,
            ],
            [
                'name'          => 'Twitter',
                'alias'         => 'twitter',
                'client_id'     => "43Z9rzBCjtPY0IGaRBSkusLpE",
                'client_secret' => 'zN1G0tm1hAZreEsgeipErF8Qz20DPPoO6wEx3dlayQvpwDissO',
                'icon'          => 'twitter',
                'is_active'     => true,
            ],
            [
                'name'          => 'Google',
                'alias'         => 'google',
                'client_id'     => "870104511574-g98geb427iu5itk1d823a4pmpiss6nl1.apps.googleusercontent.com",
                'client_secret' => 'CipV2h_pCR2YH40XUbdGHxJi',
                'icon'          => 'google',
                'is_active'     => true,
            ],
            //            [
            //                'name'          => 'Facebook',
            //                'alias'         => 'facebook',
            //                'handler'       => \App\Services\Oauth\OauthFbService::class,
            //                'client_id'     => 181070339235298,
            //                'client_secret' => 'e8fa48d8a6003c28338b1f4d30756ece',
            //                'client_redirect_url' => env( 'APP_URL' ) . '/oauth/%s/callback',
            //                'is_active'     => true,
            //            ],
        ];

        DB::table( 'networks' )->insert( $networks );

        foreach ( $networks as $network ) {
            setEnv( strtoupper( $network[ 'alias' ] . '_client_id' ), $network[ 'client_id' ] );
            setEnv( strtoupper( $network[ 'alias' ] . '_client_secret' ), $network[ 'client_secret' ] );
        }
    }
}

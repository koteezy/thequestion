<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run ()
    {
        try {
            $disk = \Illuminate\Support\Facades\Storage::disk( env( 'FILESYSTEM_DRIVER' ) );
            rrmdir( $disk->getAdapter()->getPathPrefix() );
        } catch ( Exception $exception ) {
            \Illuminate\Support\Facades\Log::info( $exception->getMessage() );
        }

        $this->call( SettingsTableSeeder::class );
        $this->call( NetworksTableSeeder::class );
        $this->call( TagsTableSeeder::class );
        $this->call( \Encore\Admin\Auth\Database\AdminTablesSeeder::class );
        $this->call( AdminMenusSeeder::class );

        if ( env( 'APP_SEEDS' ) ) {
            factory( \App\User::class )->create( [
                'avatar' => null,
            ] );
            factory( \App\User::class, 20 )->create();
            factory( \App\Models\Question\Question::class, 20 )
                ->create()
                ->each( function ( $q ) {
                    factory( \App\Models\Question\QuestionAnswer::class, random_int( 1, 8 ) )->create( [
                        'question_id' => $q->id,
                    ] );
                    $tags = \App\Models\Tag\Tag::inRandomOrder()->take( random_int( 1, 5 ) )->get();

                    $q->tags()->sync( $tags->pluck( 'id' ) );
                } );
        }
    }
}

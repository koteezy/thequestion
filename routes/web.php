<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//auth()->loginUsingId( 1 );

// create question
// auth with social networks.
//
//dd(\Illuminate\Support\Facades\Storage::url('test.png'));
Auth::routes();

Route::group( [ 'middleware' => 'auth' ], function () {
    /**
     * User area
     */
    Route::get( '/me', 'UserController@getSettings' )->name( 'me' );
    Route::post( '/me', 'UserController@updateSettings' );
    /**
     * End user area.
     */
    Route::get( '/me/questions', 'UserController@getMyQuestions' )->name( 'my_questions' );

    /**
     * Question create/edit.
     */
    Route::get( '/ask/{id?}', 'QuestionController@createQuestionView' )->name( 'ask' );
    Route::post( '/ask/{question?}', 'QuestionController@createQuestion' );
    /**
     * End question create/edit.
     */


    Route::get( '/logout', function () {
        auth()->logout();

        return redirect( '/' );
    } )->name( 'logout' );
} );

//Route::group( [ 'middleware' => 'guest' ], function () {

Route::group( [ 'prefix' => 'oauth/{oauthNetwork}', 'middleware' => 'network' ], function () {
    Route::get( '/', 'Auth\\OauthController@redirect' )->name( 'oauth' )
         ->where( 'oauthNetwork', '[a-zA-Z]+$' );
    Route::get( '/callback', 'Auth\\OauthController@callback' )->name( 'oauth.callback' )
         ->where( 'oauthNetwork', '[a-zA-Z]+$' );
} );


//} );

Route::get( '/{top?}', 'IndexController@getFeed' )
     ->where( 'top', '(top)' )->name( 'index' );
Route::get( '/feed', 'IndexController@getMyFeed' )
     ->where( 'top', '(top)' )->name( 'feed' )
     ->middleware( 'auth' );

/**
 * Questions.
 */
Route::group( [ 'prefix' => '/questions' ], function () {
    Route::get( '{id}', 'QuestionController@getQuestion' )
         ->name( 'question' )
         ->where( 'id', '[0-9]+$' );
    Route::get( '{question}/delete', 'QuestionController@deleteQuestion' )->name( 'question.delete' )
         ->middleware( 'auth' );
    Route::post( '{question}/answer', 'QuestionController@answerTheQuestion' )
         ->name( 'question.answer' )->middleware( 'auth' )
         ->where( 'question', '^[0-9]+$' );
    Route::post( '{question}/like', 'QuestionController@likeQuestion' )
         ->middleware( 'auth' )
         ->where( 'question', '^[0-9]+$' );
    Route::post( '{question}/follow', 'QuestionController@followQuestion' )
         ->middleware( 'auth' )
         ->where( 'question', '^[0-9]+$' );
} );
/**
 * End questions.
 */

/**
 * Tags
 */
Route::group( [ 'prefix' => 'tags' ], function () {
    Route::get( '', 'TagController@getTags' )->name( 'tags' );
    Route::get( '{tag}', 'TagController@getTag' )
         ->name( 'tag' )->where( 'tag', '^[0-9]+$' );
    Route::post( '{tag}/follow', 'TagController@followTag' )->name( 'tag.follow' )
         ->middleware( 'auth' )->where( 'tag', '^[0-9]+$' );
} );
/**
 * End tags
 */

/**
 * Need a create layout first..
 */
Route::get( '/users/{user}/', 'UserController@getUser' )->name( 'user' )
     ->where( 'user', '^[0-9]+$' );
//Route::get( '/users/{user}/answers', 'UserController@getUser' )->name( 'user.answers' );
//Route::get( '/users/{user}/followers', 'UserController@getUser' );
//Route::get( '/users/{user}/following', 'UserController@getUser' );


Route::post( '/answers/{answer}/rating', 'AnswerController@changeRating' )->middleware( 'auth' )
     ->where( 'answer', '^[0-9]+$' );

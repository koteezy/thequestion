<?php

namespace App\Builders;

use App\Models\Question\Question;
use App\Models\Tag\Tag;
use App\Services\UploadService;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;

class QuestionBuilder
{
    /**
     * @var Model
     */
    private $model;

    /**
     * @var array
     */
    private $data;

    /**
     * @var UploadService
     */
    private $uploadService;

    /**
     * @var RedirectResponse
     */
    private $redirect;

    /**
     * QuestionBuilder constructor.
     * @param UploadService $uploadService
     */
    public function __construct ( UploadService $uploadService )
    {
        $this->uploadService = $uploadService;
    }

    /**
     * Set question
     *
     * @param Question|null $question
     * @return $this
     */
    public function question ( ?Question $question = null )
    {
        $this->model = $question ?? new Question();

        return $this;
    }

    /**
     * Set data
     *
     * @param FormRequest $request
     * @return $this
     */
    public function validated ( FormRequest $request ): QuestionBuilder
    {
        $this->data = $request->validated();

        return $this;
    }

    public function redirect ( RedirectResponse $redirect ): QuestionBuilder
    {
        $this->redirect = $redirect->with( 'success', trans( 'messages.questions.success_save' ) );

        return $this;
    }

    public function store ()
    {
        $isUpdate = $this->model->id !== null;

        $data = array_merge( $this->data, [
            'user_id'       => u()->id,
            'was_moderated' => !s()->moderate_questions,
        ] );
        $file = $data[ 'avatar' ] ?? null;

        if ( $file ) {

            if ( $this->model->avatar )
                $this->uploadService->unset( $this->model->avatar );

            $data[ 'avatar' ] = $this->uploadService->upload( $file );
        }

        $this->model->fill( $data );
        $this->model->save();
        $this->model->tags()->sync( $this->getTags( $data[ 'tags' ] ) );

        return $this->redirect->with( 'success', $this->getFlashMessage( $isUpdate ) );
    }

    /**
     * @param bool $isUpdate
     * @return array|\Illuminate\Contracts\Translation\Translator|null|string
     */
    private function getFlashMessage ( bool $isUpdate )
    {
        if ( s()->moderate_questions ) {
            $key = 'success_create_moderate';
        } else {
            if ( $isUpdate )
                $key = 'success_update';
            else
                $key = 'success_create';
        }

        return trans( 'messages.questions.' . $key );
    }

    /**
     * @param string $tags
     * @return array
     */
    private function getTags ( string $tags )
    {
        $tags         = explode( ',', $tags );
        $existingTags = Tag::whereIn( 'name', $tags )->get();
        $newTags      = [];

        foreach ( $tags as $tag ) {
            if ( !$existingTags->contains( 'name', $tag ) ) {
                $newTags[] = trim( $tag );
            }
        }

        if ( !count( $existingTags ) )
            $newTags = $tags;

        foreach ( $newTags as $tag ) {
            $existingTags->push( Tag::create( [
                'name' => $tag,
            ] ) );
        }

        return $existingTags->pluck( 'id' );
    }

}
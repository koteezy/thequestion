<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class QuestionTagsRule implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct ()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string $attribute
     * @param  mixed $value
     * @return bool
     */
    public function passes ( $attribute, $value )
    {
        $validated = true;
        $tags      = explode( ',', $value );

        foreach ( $tags as $tag ) {
            if ( mb_strlen( $tag ) > 50 )
                $validated = false;
        }

        if ( count( $tags ) > 5 )
            $validated = false;

        return $validated && count( $tags );
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message ()
    {
        return trans( 'validation.custom.question_tag' );
    }
}

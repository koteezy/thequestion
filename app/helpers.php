<?php

/**
 * Get storage file url
 *
 * @param null|string $path
 * @param null|string $default
 * @return string
 */
function furl ( ?string $path = null, ?string $default = null ): string
{
    if ( !$path )
        return $default;

    return Storage::url( $path );
}

/**
 * Retrieve user avatar
 *
 * @param null|string $path
 * @return string
 */
function uv ( ?string $path = null )
{
    return furl( $path, asset( '/assets/img/default_avatar.jpeg' ) );
}

/**
 * Get current auth user instance.
 *
 * @return \Illuminate\Contracts\Auth\Authenticatable|null
 */
function u ()
{
    return auth()->user();
}

function s ()
{
    return \App\Models\Setting::first();
}

/**
 * @param string $url
 * @return string
 */
function uploadRandomImage ( string $url ): string
{

    $originalName = md5( str_random( 20 ) ) . '.jpg';
    $info         = pathinfo( $url );
    $contents     = file_get_contents( $url );
    $file         = '/tmp/' . $info[ 'basename' ];
    file_put_contents( $file, $contents );

    $file = new \Illuminate\Http\UploadedFile( $file, $originalName );

    return ( new \App\Services\UploadService() )->upload( $file );
}

/**
 *
 * @author https://stackoverflow.com/a/3338133/9844233
 * @param $dir
 */
function rrmdir ( $dir )
{
    if ( is_dir( $dir ) ) {
        $objects = scandir( $dir );
        foreach ( $objects as $object ) {
            if ( $object != "." && $object != ".." ) {
                if ( is_dir( $dir . "/" . $object ) )
                    rrmdir( $dir . "/" . $object );
                else
                    unlink( $dir . "/" . $object );
            }
        }
        rmdir( $dir );
    }
}

function setEnv($key, $newValue)
{
    $delim='';
    $path = base_path('.env');

    // get old value from current env
    $oldValue = env($key);

    // was there any change?
    if ($oldValue === $newValue) {
        return;
    }

    // rewrite file content with changed data
    if (file_exists($path)) {
        // replace current value with new value
        file_put_contents(
            $path, str_replace(
                $key.'='.$delim.$oldValue.$delim,
                $key.'='.$delim.$newValue.$delim,
                file_get_contents($path)
            )
        );
    }
}

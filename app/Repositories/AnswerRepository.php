<?php

namespace App\Repositories;

use App\Models\Question\QuestionAnswer;
use App\Models\Tag\Tag;
use Illuminate\Support\Facades\DB;

class AnswerRepository extends BaseRepository
{
    public function __construct ( QuestionAnswer $tag )
    {
        parent::__construct( $tag );
    }

    public function getQuestionAnswers ( int $questionId )
    {
        $userId = auth()->id();
        $rows   = [
            'a.id',
            'a.text',
            'a.created_at',
            'u.id as user_id',
            'u.avatar as user_avatar',
            'u.first_name as user_first_name',
            'u.last_name as user_last_name',
            DB::raw( '(select COALESCE(sum(sign), 0) from question_answer_likes where answer_id = a.id) as rating' ),
        ];

        if ( $userId )
            $rows[] = DB::raw( "(select sign from question_answer_likes where answer_id = a.id and user_id = $userId) as rated_as" );

        return $this->model->from( 'question_answers as a' )->select( $rows )
                           ->join( 'users as u', 'u.id', 'a.user_id' )
                           ->where( 'a.question_id', $questionId )
                           ->orderBy( 'rating', 'desc' )
                           ->orderBy( 'created_at', 'asc' )
                           ->get();
    }
}
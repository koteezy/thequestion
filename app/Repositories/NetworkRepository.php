<?php

namespace App\Repositories;

use App\Models\Network;
use Illuminate\Database\Eloquent\Collection;

class NetworkRepository extends BaseRepository
{
    public function __construct ( Network $network )
    {
        parent::__construct( $network );
    }

    /**
     * Get social networks for settings page.
     *
     * @return Network|Collection
     */
    public function getNetworks ()
    {
        $rows = [ 'n.name', 'n.alias', 'n.icon' ];

        $builder = $this->model->from( 'networks as n' )
                               ->select( $rows )
                               ->where( 'n.is_active', true );

        if ( u() ) {
            $builder = $builder->leftJoin( 'user_networks as un', function ( $join ) {
                $join->on( 'un.network_id', 'n.id' )
                     ->where( 'un.user_id', u()->id );
            } )->addSelect( 'un.uuid' );
        }

        return $builder->get();
    }
}
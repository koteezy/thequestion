<?php

namespace App\Repositories;

use App\Models\Question\Question;
use App\Traits\Repositories\QuestionRepositoryFilter;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\DB;

class QuestionRepository extends BaseRepository
{
    use QuestionRepositoryFilter;

    /**
     * QuestionRepository constructor.
     * @param Question $question
     */
    public function __construct ( Question $question )
    {
        parent::__construct( $question );
    }


    /**
     * Set base builder instance.
     *
     * @param bool $onlyModerated
     * @return Builder
     */
    public function setQuestions ( bool $onlyModerated = false ): Builder
    {
        $userId = auth()->id();
        $rows   = [
            'q.id',
            'q.question',
            'q.avatar',
            'q.was_moderated',
            'q.created_at',
            'u.id as user_id',
            'u.avatar as user_avatar',
            'u.first_name as user_first_name',
            'u.last_name as user_last_name',
            DB::raw( "(select count(question_id) from question_likes where question_id = q.id) as likes_count" ),
            DB::raw( "(select count(question_id) from question_followers where question_id = q.id) as followers_count" ),
            DB::raw( '(select count(question_id) from question_views where question_id = q.id) as views_count' ),
            DB::raw( '(select count(question_id) from question_answers where question_id = q.id) as answers_count' ),
        ];

        if ( $userId ) {
            $rows[] = DB::raw( "(select count(question_id) from question_likes where question_id = q.id and user_id = $userId) as is_liked" );
        }

        $this->model = $this->model->from( 'questions as q' )
                                   ->join( 'users as u', 'u.id', 'q.user_id' )
                                   ->select( $rows );

        if ( $onlyModerated )
            $this->moderated();

        return $this->model;
    }


    /**
     * @return $this
     */
    public function feedQuestions (): QuestionRepository
    {
        $this->model = $this->setQuestions( true )
                            ->orderBy( 'q.created_at', 'desc' );

        return $this;
    }

    /**
     * Get user feed, that is, the questions that contain the tags that the user subscribed to.
     *
     * @param int $userId
     * @return $this
     */
    public function userFeedQuestions ( int $userId ): QuestionRepository
    {
        $this->model = $this->setQuestions( true )
                            ->whereRaw( "exists ( 
                             select qt.question_id from question_tags as qt
                             join tag_followers as t on t.tag_id = qt.tag_id where t.user_id = $userId
                             and qt.question_id = q.id
                            )
                            " );

        return $this;
    }

    /**
     * Get questions of specific user.
     *
     * @param int $userId
     * @return $this
     */
    public function userQuestions ( int $userId ): QuestionRepository
    {
        $this->model = $this->setQuestions()->where( 'q.user_id', $userId );

        return $this;
    }


    /**
     * @param $tagId
     * @return $this
     */
    public function tagQuestions ( $tagId ): QuestionRepository
    {
        $this->model = $this->setQuestions( true )
                            ->whereRaw( "exists ( select question_id from question_tags where tag_id = $tagId and question_id = q.id)" );

        return $this;
    }

    /**
     * @param int $id
     * @return Question|\Illuminate\Database\Eloquent\Model|static
     */
    public function getQuestion ( int $id )
    {
        $question = $this->setQuestions()->where( 'q.id', $id );

        if ( $id = auth()->id() ) {
            $question->addSelect( [
                DB::raw( "(select user_id from question_followers where question_id = q.id and user_id = $id) as im_followed" ),
                DB::raw( "(select user_id from question_answers where question_id = q.id and user_id = $id) as answered_by_me" ),
            ] );
        }

        return $question->firstOrFail();
    }
}
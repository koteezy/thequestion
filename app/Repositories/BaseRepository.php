<?php

namespace App\Repositories;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

/**
 * @property Builder $model
 *
 * Class BaseRepository
 * @package App\Repositories
 */
class BaseRepository
{
    /**
     * @var Builder
     */
    protected $model;

    /**
     * BaseRepository constructor.
     * @param Model $model
     */
    public function __construct ( Model $model )
    {
        $this->model = $model;
    }

    public function get ()
    {
        return $this->model->get();
    }

    public function paginate ( int $perPage = 15 )
    {
        return $this->model->paginate( $perPage );
    }
}
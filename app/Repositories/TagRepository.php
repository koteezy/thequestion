<?php

namespace App\Repositories;

use App\Models\Tag\Tag;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\DB;

class TagRepository extends BaseRepository
{
    public function __construct ( Tag $tag )
    {
        parent::__construct( $tag );
    }

    /**
     * Get base query.
     *
     * @return Builder
     */
    public function setTags ()
    {
        $userId = auth()->id();
        $rows   = [
            't.id',
            't.name',
            't.color',
            DB::raw( '(select count(tag_id) from question_tags where tag_id = t.id) as questions_count' ),
            DB::raw( '(select count(tag_id) from tag_followers where tag_id = t.id) as followers_count' ),
        ];

        if ( $userId ) {
            $rows[] = DB::raw( "(select user_id from tag_followers where tag_id = t.id and user_id = $userId) as im_followed" );
        }

        return $this->model->from( 'tags as t' )
                           ->select( $rows )
                           ->where( 't.is_accepted', true );
    }


    /**
     * @param $id
     * @return Builder|\Illuminate\Database\Eloquent\Model
     */
    public function getTag ( $id )
    {
        return $this->setTags()->where( 't.id', $id )->firstOrFail();
    }

    /**
     * @return $this
     */
    public function getTags (): TagRepository
    {
        $this->model = $this->setTags()->orderBy( 'followers_count', 'desc' );

        return $this;
    }
}
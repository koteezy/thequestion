<?php

namespace App\Http\Middleware;

use Closure;

class GenerateUniqueHash
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle ( $request, Closure $next )
    {
        if ( $request->hasCookie( 'u_hash' ) ) {
            return $next( $request );
        }

        $uuid = bcrypt( str_random( 30 ) . date( 'Y-m-d h:i:s' ) );

        return redirect( $request->url() )->withCookie( cookie()->forever( 'u_hash', $uuid ) );
//        if(! $request->hasCookie('u_hash'))
//            $request->cookie('u_hash', '')
//
//        return $next($request);
    }
}

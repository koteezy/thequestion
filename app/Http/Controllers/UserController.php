<?php

namespace App\Http\Controllers;

use App\Http\Requests\UpdateUserRequest;
use App\Repositories\QuestionRepository;
use App\Services\Ajaxify\AjaxifyIndexService;
use App\Services\Ajaxify\Me\AjaxifyProfileSettingsService;
use App\Services\UploadService;
use Illuminate\Http\Request;

class UserController extends Controller
{
    /**
     * @var UploadService
     */
    private $uploadService;
    /**
     * @var QuestionRepository
     */
    private $questionRepository;
    /**
     * @var AjaxifyProfileSettingsService
     */
    private $ajaxifyProfileSettingsService;
    /**
     * @var AjaxifyIndexService
     */
    private $ajaxifyIndexService;

    /**
     * UserController constructor.
     * @param UploadService $uploadService
     * @param QuestionRepository $questionRepository
     * @param AjaxifyProfileSettingsService $ajaxifyProfileSettingsService
     * @param AjaxifyIndexService $ajaxifyIndexService
     */
    public function __construct (
        UploadService $uploadService,
        QuestionRepository $questionRepository,
        AjaxifyProfileSettingsService $ajaxifyProfileSettingsService,
        AjaxifyIndexService $ajaxifyIndexService )
    {
        $this->uploadService                 = $uploadService;
        $this->questionRepository            = $questionRepository;
        $this->ajaxifyProfileSettingsService = $ajaxifyProfileSettingsService;
        $this->ajaxifyIndexService           = $ajaxifyIndexService;
    }

    public function getUser ( $id )
    {
        return 'In the making';
    }

    /**
     * Get user questions.
     *
     * @return array|\Illuminate\Contracts\View\View
     */
    public function getMyQuestions ()
    {
        return $this->ajaxifyIndexService->myQuestions()->render();
    }

    /**
     * Get settings page
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getSettings ()
    {
        return $this->ajaxifyProfileSettingsService->render();
    }

    /**
     * Update user base information.
     *
     * @param UpdateUserRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function updateSettings ( UpdateUserRequest $request )
    {
        $data = $request->validated();

        if ( $request->hasFile( 'avatar' ) ) {

            if ( u()->avatar )
                $this->uploadService->unset( u()->avatar );

            $data[ 'avatar' ] = $this->uploadService->upload( $request->file( 'avatar' ) );
        }

        if ( isset( $data[ 'password' ] ) ) {
            $data[ 'password' ] = bcrypt( $data[ 'password' ] );
        }

        u()->update( $data );

        return redirect()->back()->with( 'success', trans( 'messages.user.profile_updated' ) );
    }
}

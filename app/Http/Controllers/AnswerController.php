<?php

namespace App\Http\Controllers;

use App\Http\Requests\ChangeAnswerRatingRequest;
use App\Models\Question\QuestionAnswer;
use Illuminate\Http\Request;

class AnswerController extends Controller
{
    /**
     * Change answer rating, or remove our previous vote.
     *
     * @param QuestionAnswer $answer
     * @param ChangeAnswerRatingRequest $request
     *
     * @return int
     */
    public function changeRating ( QuestionAnswer $answer, ChangeAnswerRatingRequest $request )
    {
        $data = $request->validated();

        $answer->likes()->detach( auth()->id() );

        if ( isset( $data[ 'sign' ] ) ) {
            $answer->likes()->attach( auth()->id(), $data );
        }

        return $answer->likes()->sum( 'sign' );
    }
}

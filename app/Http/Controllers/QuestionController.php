<?php

namespace App\Http\Controllers;

use App\Builders\QuestionBuilder;
use App\Http\Requests\AnswerQuestionRequest;
use App\Http\Requests\CreateQuestionRequest;
use App\Models\Question\Question;
use App\Services\Ajaxify\AjaxifyQuestionService;
use App\Services\Ajaxify\AjaxifyAskService;
use App\Services\SyncService;
use Illuminate\Http\RedirectResponse;

class QuestionController extends Controller
{
    /**
     * @var AjaxifyQuestionService
     */
    private $ajaxifyQuestionService;
    /**
     * @var SyncService
     */
    private $syncService;
    /**
     * @var QuestionBuilder
     */
    private $questionBuilder;
    /**
     * @var AjaxifyAskService
     */
    private $ajaxifyAskService;

    /**
     * QuestionController constructor.
     * @param AjaxifyQuestionService $ajaxifyQuestionService
     * @param SyncService $syncService
     * @param QuestionBuilder $questionBuilder
     * @param AjaxifyAskService $ajaxifyAskService
     */
    public function __construct ( AjaxifyQuestionService $ajaxifyQuestionService, SyncService $syncService, QuestionBuilder $questionBuilder, AjaxifyAskService $ajaxifyAskService )
    {
        $this->ajaxifyQuestionService = $ajaxifyQuestionService;
        $this->syncService            = $syncService;
        $this->questionBuilder        = $questionBuilder;
        $this->ajaxifyAskService      = $ajaxifyAskService;
    }

    public function getQuestion ( int $id )
    {
        return $this->ajaxifyQuestionService->render( $id );
    }

    /**
     * Like or unlike question
     *
     * @param Question $question
     */
    public function likeQuestion ( Question $question )
    {
        $this->syncService->as( auth()->id() )->to( $question->likes() );
    }

    /**
     * Follow or unfollow from question
     *
     * @param Question $question
     */
    public function followQuestion ( Question $question )
    {
        $this->syncService->as( auth()->id() )->to( $question->followers() );
    }

    /**
     * @param Question $question
     * @param AnswerQuestionRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function answerTheQuestion ( Question $question, AnswerQuestionRequest $request )
    {
        $question->answers()->create(
            array_merge( $request->validated(), [ 'user_id' => u()->id ] )
        );

        return redirect()->back()->with( 'success', trans( 'messages.questions.answer_created' ) );
    }

    /**
     * Show edit/create question form.
     *
     * @param int|null $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function createQuestionView ( int $id = null )
    {
        return $this->ajaxifyAskService->setQuestion( $id )->render();
    }

    /**
     * @param Question|null $question
     * @param CreateQuestionRequest $request
     * @return RedirectResponse
     */
    public function createQuestion ( ?Question $question = null, CreateQuestionRequest $request )
    {
        return $this->questionBuilder->question( $question )->validated( $request )
                                     ->redirect( redirect()->back() )
                                     ->store();
    }

    /**
     * Delete question.
     *
     * @param Question $question
     * @return RedirectResponse
     * @throws \Exception
     */
    public function deleteQuestion ( Question $question )
    {
        if ( u()->id === $question->user_id )
            $question->delete();
        else
            abort( 401 );

        return redirect()->back()->with( 'success', trans( 'messages.questions.success_delete' ) );
    }
}

<?php

namespace App\Http\Controllers;

use App\Services\Ajaxify\AjaxifyIndexService;
use Illuminate\Http\Request;

class IndexController extends Controller
{
    /**
     * @var AjaxifyIndexService
     */
    private $ajaxifyIndexService;

    /**
     * FeedController constructor.
     * @param AjaxifyIndexService $ajaxifyIndexService
     */
    public function __construct ( AjaxifyIndexService $ajaxifyIndexService )
    {
        $this->ajaxifyIndexService = $ajaxifyIndexService;
    }

    /**
     * Get global feed
     *
     * @param null $top
     * @return array|\Illuminate\Contracts\View\View
     */
    public function getFeed ( $top = null )
    {
        return $this->ajaxifyIndexService->feedQuestions( $top )->render();
    }

    /**
     * Get user feed.
     *
     * @param null $top
     * @return array|\Illuminate\Contracts\View\View
     */
    public function getMyFeed ( $top = null )
    {
        return $this->ajaxifyIndexService->userFeedQuestions( $top )->render();
    }
}

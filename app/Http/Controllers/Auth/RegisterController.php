<?php

namespace App\Http\Controllers\Auth;

use App\Models\Network;
use App\Services\Ajaxify\Auth\AjaxifyRegisterService;
use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * @var AjaxifyRegisterService
     */
    private $ajaxifyRegisterService;
    /**
     * @var Request
     */
    private $request;

    /**
     * Create a new controller instance.
     *
     * @param AjaxifyRegisterService $ajaxifyRegisterService
     * @param Request $request
     */
    public function __construct ( AjaxifyRegisterService $ajaxifyRegisterService, Request $request )
    {
        $this->middleware( 'guest' );

        $this->ajaxifyRegisterService = $ajaxifyRegisterService;
        $this->request                = $request;
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator ( array $data )
    {
        return Validator::make( $data, [
            'first_name' => 'required|string|max:255',
            'last_name'  => 'required|string|max:255',
            'email'      => 'required|string|email|max:255|unique:users',
            'password'   => 'required|string|min:6',
        ] );
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array $data
     * @return \App\User
     */
    protected function create ( array $data )
    {
        return User::create( [
            'first_name' => $data[ 'first_name' ],
            'last_name'  => $data[ 'last_name' ],
            'email'      => $data[ 'email' ],
            'password'   => Hash::make( $data[ 'password' ] ),
        ] );
    }

    /**
     * @return array|\Illuminate\Contracts\View\View
     */
    public function showRegistrationForm ()
    {
        return $this->ajaxifyRegisterService->render();
    }

    /**
     * Handle a registration request for the application.
     *
     * @return \Illuminate\Http\Response
     */
    public function register ()
    {
        $user = $this->request->all();

        $this->validator( $user )->validate();

        if ( $alias = $this->hasNetworkCookie() ) {

            Cookie::queue(
                Cookie::forget( 'network_id' )
            );

            return redirect( route( 'oauth', $alias ) )->withCookie( 'user', serialize( $user ) );
        }

        $user = $this->create( $this->request->all() );

        $this->guard()->login( $user );

        return redirect( $this->redirectPath() );
    }


    /**
     * If there is a social network cookie,
     * then you need to try to attach this network to account.
     *
     * @return string|null
     */
    private function hasNetworkCookie ()
    {
        $network = Network::find( $this->request->cookie( 'network_id' ) );

        return $network && $network->is_active ? $network->alias : null;
    }
}

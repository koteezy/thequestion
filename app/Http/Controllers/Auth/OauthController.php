<?php

namespace App\Http\Controllers\Auth;

use App\Models\Network;
use App\Services\OauthService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Cookie;

class OauthController extends Controller
{
    /**
     * @param Network $oauthNetwork
     * @param Request $request
     * @return mixed
     */
    public function redirect ( Network $oauthNetwork, Request $request )
    {
        if ( $request->has( 'redirect' ) ) {
            Cookie::queue(
                cookie( 'redirect', $request->get( 'redirect' ), 2 )
            );
        }

        return ( new OauthService( $oauthNetwork ) )->redirect();
    }

    /**
     * @param Network $oauthNetwork
     * @return \Illuminate\Http\RedirectResponse
     */
    public function callback ( Network $oauthNetwork )
    {
        return ( new OauthService( $oauthNetwork ) )->callback();
    }
}

<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Services\Ajaxify\Auth\AjaxifyLoginService;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class LoginController extends Controller
{
    /**
     * @var AjaxifyLoginService
     */
    private $ajaxifyLoginService;

    /**
     * LoginController constructor.
     * @param AjaxifyLoginService $ajaxifyLoginService
     */
    public function __construct ( AjaxifyLoginService $ajaxifyLoginService )
    {
        $this->middleware( 'guest' )->except( 'logout' );

        $this->ajaxifyLoginService = $ajaxifyLoginService;
    }

    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * @return array|\Illuminate\Contracts\View\View
     */
    public function showLoginForm ()
    {
        return $this->ajaxifyLoginService->render();
    }
}

<?php

namespace App\Http\Controllers;

use App\Models\Tag\Tag;
use App\Services\Ajaxify\Tags\AjaxifyTagService;
use App\Services\Ajaxify\Tags\AjaxifyTagsService;
use App\Services\SyncService;
use Illuminate\Http\Request;

class TagController extends Controller
{
    /**
     * @var AjaxifyTagService
     */
    private $ajaxifyTagService;
    /**
     * @var AjaxifyTagsService
     */
    private $ajaxifyTagsService;
    /**
     * @var SyncService
     */
    private $syncService;

    /**
     * TagController constructor.
     * @param AjaxifyTagService $ajaxifyTagService
     * @param AjaxifyTagsService $ajaxifyTagsService
     * @param SyncService $syncService
     */
    public function __construct ( AjaxifyTagService $ajaxifyTagService, AjaxifyTagsService $ajaxifyTagsService, SyncService $syncService )
    {
        $this->ajaxifyTagService  = $ajaxifyTagService;
        $this->ajaxifyTagsService = $ajaxifyTagsService;
        $this->syncService        = $syncService;
    }

    /**
     * Get single tag view
     *
     * @param $tag
     * @return array|\Illuminate\Contracts\View\View
     */
    public function getTag ( $tag )
    {
        return $this->ajaxifyTagService->render( $tag );
    }

    /**
     * List of tags.
     *
     * @return array|\Illuminate\Contracts\View\View
     */
    public function getTags ()
    {
        return $this->ajaxifyTagsService->render();
    }

    /**
     * Follow or unfollow from tag.
     *
     * @param Tag $tag
     */
    public function followTag ( Tag $tag )
    {
        $this->syncService->as( auth()->id() )->to( $tag->followers() );
    }
}

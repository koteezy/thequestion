<?php

namespace App\Http\Requests;

use App\Rules\QuestionRule;
use App\Rules\QuestionTagsRule;
use Illuminate\Foundation\Http\FormRequest;

class CreateQuestionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize ()
    {
        $bool = u();

        if ( $q = $this->route( 'question' ) )
            $bool = $bool && u() && $q->user_id === u()->id;

        return $bool;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules ()
    {
        return [
            'question' => [ 'required', 'max:80', new QuestionRule() ],
            'tags'     => [ 'required', new QuestionTagsRule() ],
            'avatar'   => 'sometimes|file|mimes:jpeg,jpg,png|max:1000',
        ];
    }
}

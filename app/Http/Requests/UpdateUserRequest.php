<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize ()
    {
        return auth()->id();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules ()
    {
        return [
            'first_name' => 'required|max:255',
            'last_name'  => 'required|max:255',
            'about'      => 'sometimes|max:255',
            'email'      => 'required|email|max:255|unique:users,email,' . u()->id,
            'avatar'     => 'sometimes|file|max:1000|mimes:jpeg,bmp,png',
            'password'   => 'sometimes:min:6',
        ];
    }
}

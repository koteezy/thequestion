<?php

namespace App\Models;

use App\Services\Oauth\OauthService;
use Illuminate\Database\Eloquent\Model;

/**
 * @property OauthService $handler
 *
 * Class Network
 * @package App\Models
 */
class Network extends Model
{
    /**
     * @return bool
     */
    public function getOauthIsReadyAttribute (): bool
    {
        return $this->is_active && $this->client_id && $this->client_secret;
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
    protected $fillable   = [ 'moderate_questions' ];
    public    $timestamps = false;
//    public $incrementing = false;
}

<?php

namespace App\Models\Tag;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
    protected $fillable = [
        'name', 'description', 'is_accepted',
    ];

    /**
     * Tag followers
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function followers ()
    {
        return $this->belongsToMany( User::class, 'tag_followers', 'tag_id', 'user_id' );
    }
}

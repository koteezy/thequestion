<?php

namespace App\Models\Question;

use Illuminate\Database\Eloquent\Model;

class QuestionView extends Model
{
    protected $fillable = [
        'question_id', 'user_id', 'ip', 'hash',
    ];
}

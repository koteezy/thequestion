<?php

namespace App\Models\Question;

use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class QuestionAnswer extends Model
{
    /**
     * @var array
     */
    protected $fillable = [
        'user_id', 'question_id', 'text',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function question ()
    {
        return $this->belongsTo( Question::class, 'question_id', 'id' );
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user ()
    {
        return $this->belongsTo( User::class, 'user_id', 'id' );
    }

    /**
     * Get all users which like that answer.
     *
     * @return BelongsToMany
     */
    public function likes ()
    {
        return $this->belongsToMany( User::class, 'question_answer_likes', 'answer_id', 'user_id' )
                    ->withPivot( [ 'sign as sign' ] );
    }
}

<?php

namespace App\Models\Question;

use App\Models\Tag\Tag;
use App\Services\Views\QuestionViewService;
use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Cookie;

class Question extends Model
{
    protected $fillable = [
        'user_id', 'question', 'was_moderated', 'avatar',
    ];

    /**
     * Ge tall tags, which assign to that question.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function tags ()
    {
        return $this->belongsToMany( Tag::class, 'question_tags', 'question_id', 'tag_id' );
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user ()
    {
        return $this->belongsTo( User::class, 'user_id', 'id' );
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function views ()
    {
        return $this->hasMany( QuestionView::class, 'question_id', 'id' );
    }

    /**
     * Add views to this question.
     */
    public function view ()
    {
        return ( new QuestionViewService( $this ) )->view();
    }

    /**
     * Get users which like that question.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function likes ()
    {
        return $this->belongsToMany( User::class, 'question_likes', 'question_id', 'user_id' );
    }

    /**
     * Get all users which follow on that question.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function followers ()
    {
        return $this->belongsToMany( User::class, 'question_followers', 'question_id', 'user_id' );
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function answers ()
    {
        return $this->hasMany( QuestionAnswer::class, 'question_id', 'id' );
    }
}

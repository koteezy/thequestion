<?php

use Illuminate\Routing\Router;

Admin::registerAuthRoutes();

Route::group( [
    'prefix'     => config( 'admin.route.prefix' ),
    'namespace'  => config( 'admin.route.namespace' ),
    'middleware' => config( 'admin.route.middleware' ),
], function ( Router $router ) {

    $router->get( '/', 'HomeController@index' );
    $router->get( '/users/search', 'UserController@search' );
    $router->get( '/questions/search', 'QuestionController@search' );
    $router->resource( 'users', 'UserController' );
    $router->resource( 'questions', 'QuestionController' );
    $router->resource( 'tags', 'TagController' );
    $router->resource( 'answers', 'AnswerController' );
    $router->resource( 'networks', 'NetworkController' );

    $router->resource( '/settings', 'SettingController' );
} );

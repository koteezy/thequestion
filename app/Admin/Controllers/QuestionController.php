<?php

namespace App\Admin\Controllers;

use App\Models\Question\Question;

use App\Models\Tag\Tag;
use App\User;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\ModelForm;
use Illuminate\Http\Request;

class QuestionController extends Controller
{
    use ModelForm;

    /**
     * For ajax select.
     *
     * @param Request $request
     * @return mixed
     */
    public function search ( Request $request )
    {
        $q = $request->get( 'q' );

        return Question::where( 'question', 'like', "%$q%" )
                       ->paginate( null, [ 'id', 'question as text' ] );
    }

    /**
     * Index interface.
     *
     * @return Content
     */
    public function index ()
    {
        return Admin::content( function ( Content $content ) {

            $content->header( 'Questions' );
//            $content->description('description');

            $content->body( $this->grid() );
        } );
    }

    /**
     * Edit interface.
     *
     * @param $id
     * @return Content
     */
    public function edit ( $id )
    {
        return Admin::content( function ( Content $content ) use ( $id ) {

            $content->header( 'Edit question' );
//            $content->description( 'description' );

            $content->body( $this->form()->edit( $id ) );
        } );
    }

    /**
     * Create interface.
     *
     * @return Content
     */
    public function create ()
    {
        return Admin::content( function ( Content $content ) {

            $content->header( 'Create question' );
//            $content->description( 'description' );

            $content->body( $this->form() );
        } );
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid ()
    {
        return Admin::grid( Question::class, function ( Grid $grid ) {

//            $grid->actions(function ($actions) {
//
//                 add action
//                $actions->append(new CheckRow($actions->getKey()));
//            });

            $grid->id( 'ID' )->sortable();
            $grid->column( 'question' )
                 ->editable( 'text' );
            $grid->column( 'user.email', 'User' )->sortable();
            $grid->tags()->display( function ( $tags ) {

                $tags = array_map( function ( $tag ) {
                    return "<span class='label label-success'>{$tag['name']}</span>&nbsp;";
                }, $tags );

                return join( '&nbsp;', $tags );
            } );
            $grid->column( 'was_moderated', 'Accepted by the moderator?' )->sortable()
                 ->editable( 'select', [ 0 => 'Declined', 1 => 'Accepted' ] );

            $grid->created_at()->sortable();
            $grid->updated_at()->sortable();
        } );
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form ()
    {
        return Admin::form( Question::class, function ( Form $form ) {

            $form->display( 'id', 'ID' );
            $form->select( 'user_id', 'User' )->options( function ( $id ) {
                $user = User::find( $id );

                if ( $user ) {
                    return [ $user->id => $user->email ];
                }
            } )->ajax( '/admin/users/search' )->rules( 'required' );
            $form->text( 'question', 'Question' )->rules( 'required|max:80' );
            $form->radio( 'was_moderated', 'Accepted by the moderator?' )
                 ->options( [ true => 'Accepted', false => 'Declined' ] )
                 ->stacked();
            $form->multipleSelect( 'tags' )->options( Tag::all()->pluck( 'name', 'id' ) );
            $form->file( 'avatar', 'Avatar' )->disk( env( 'FILESYSTEM_DRIVER' ) )->name( md5( str_random( 200 ) ) );

            $form->datetime( 'created_at', 'Created At' );
            $form->datetime( 'updated_at', 'Updated At' );
        } );
    }
}

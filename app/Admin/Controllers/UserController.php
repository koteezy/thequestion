<?php

namespace App\Admin\Controllers;

use App\User;

use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\ModelForm;
use Illuminate\Http\Request;

class UserController extends Controller
{
    use ModelForm;

    public function search ( Request $request )
    {
        $q = $request->get( 'q' );

        return User::where( 'email', 'like', "%$q%" )
                   ->paginate( null, [ 'id', 'first_name', 'last_name', 'email as text' ] );
    }

    /**
     * Index interface.
     *
     * @return Content
     */
    public function index ()
    {
        return Admin::content( function ( Content $content ) {

            $content->header( 'Users' );
//            $content->description( 'description' );

            $content->body( $this->grid() );
        } );
    }

    /**
     * Edit interface.
     *
     * @param $id
     * @return Content
     */
    public function edit ( $id )
    {
        return Admin::content( function ( Content $content ) use ( $id ) {

            $content->header( 'Edit user' );
//            $content->description( 'description' );

            $content->body( $this->form()->edit( $id ) );
        } );
    }

    /**
     * Create interface.
     *
     * @return Content
     */
    public function create ()
    {
        return Admin::content( function ( Content $content ) {

            $content->header( 'Create user' );
//            $content->description( 'description' );

            $content->body( $this->form() );
        } );
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid ()
    {
        return Admin::grid( User::class, function ( Grid $grid ) {

            $grid->id( 'ID' )->sortable();
            $grid->column( 'first_name', 'First name' )
                 ->editable( 'text' );
            $grid->column( 'last_name', 'Last name' )
                 ->editable( 'text' );
            $grid->column( 'email', 'Email' )
                 ->editable( 'text' );
            $grid->column( 'about', 'About' );

            $grid->created_at();
            $grid->updated_at();
        } );
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form ()
    {
        return Admin::form( User::class, function ( Form $form ) {

            $form->display( 'id', 'ID' );
            $form->text( 'first_name', 'First name' )->rules( 'required|max:255' );
            $form->text( 'last_name', 'Last name' )->rules( 'required|max:255' );
            $form->email( 'email', 'Email' )->rules( 'required|email|max:255' );
            $form->textarea( 'about', 'About' )->rules( 'max:255' );
            $form->file( 'avatar', 'Avatar' )->disk( env( 'FILESYSTEM_DRIVER' ) )->name( md5( str_random( 200 ) ) );
            $form->password( 'password', 'Password' );

            $form->display( 'created_at', 'Created At' );
            $form->display( 'updated_at', 'Updated At' );

            $form->saving( function ( Form $form ) {
                $password = $form->password;

                if ( strlen( $password ) > 0 ) {
                    $form->password = bcrypt( $password );
                }
            } );
        } );
    }
}

<?php

namespace App\Admin\Controllers;

use App\Models\Setting;

use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\ModelForm;

class SettingController extends Controller
{
    use ModelForm;

    /**
     * Index interface.
     *
     * @return Content
     */
    public function index ()
    {
        return Admin::content( function ( Content $content ) {

            $content->header( 'header' );
            $content->description( 'description' );

            $content->body( $this->grid() );
        } );
    }

    /**
     * Edit interface.
     *
     * @param $id
     * @return Content
     */
    public function edit ( $id )
    {
        return Admin::content( function ( Content $content ) use ( $id ) {

            $content->header( 'Edit settings' );
//            $content->description('description');

            $content->body( $this->form()->edit( $id ) );
        } );
    }

    /**
     * Create interface.
     *
     * @return Content
     */
    public function create ()
    {
        return Admin::content( function ( Content $content ) {

            $content->header( 'header' );
            $content->description( 'description' );

            $content->body( $this->form() );
        } );
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid ()
    {
        return Admin::grid( Setting::class, function ( Grid $grid ) {

            $grid->id( 'ID' )->sortable();

            $grid->created_at();
            $grid->updated_at();
        } );
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form ()
    {
        return Admin::form( Setting::class, function ( Form $form ) {

//            $form->display( 'id', 'ID' );
            $form->text( 'site_title', 'Site name' );
            $form->text( 'site_title_separator', 'Site name separator' );
            $form->radio( 'moderate_questions', 'Moderate all questions after create/update?' )
                 ->options( [ false => 'Do not moderate', true => 'Moderate' ] );

            $form->saved( function ( Form $form ) {
                return redirect( '/admin' );
            } );

//            $form->display( 'created_at', 'Created At' );
//            $form->display( 'updated_at', 'Updated At' );
        } );
    }
}

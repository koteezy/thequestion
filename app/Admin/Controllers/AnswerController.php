<?php

namespace App\Admin\Controllers;

use App\Models\Question\Question;
use App\Models\Question\QuestionAnswer;

use App\User;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\ModelForm;

class AnswerController extends Controller
{
    use ModelForm;

    /**
     * Index interface.
     *
     * @return Content
     */
    public function index ()
    {
        return Admin::content( function ( Content $content ) {

            $content->header( 'Question answers' );
//            $content->description('description');

            $content->body( $this->grid() );
        } );
    }

    /**
     * Edit interface.
     *
     * @param $id
     * @return Content
     */
    public function edit ( $id )
    {
        return Admin::content( function ( Content $content ) use ( $id ) {

            $content->header( 'Edit answer' );
//            $content->description( 'description' );

            $content->body( $this->form()->edit( $id ) );
        } );
    }

    /**
     * Create interface.
     *
     * @return Content
     */
    public function create ()
    {
        return Admin::content( function ( Content $content ) {

            $content->header( 'Create answer' );
//            $content->description( 'description' );

            $content->body( $this->form() );
        } );
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid ()
    {
        return Admin::grid( QuestionAnswer::class, function ( Grid $grid ) {

            $grid->id( 'ID' )->sortable();
            $grid->column( 'question.question', 'Question' )->sortable();
            $grid->column( 'user.email', 'User' )->sortable();
            $grid->column( 'text', 'Answer' )->sortable();

            $grid->created_at();
            $grid->updated_at();
        } );
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form ()
    {
        return Admin::form( QuestionAnswer::class, function ( Form $form ) {

            $form->display( 'id', 'ID' );
            $form->textarea( 'text', 'Answer' );
            $form->select( 'user_id', 'User' )->options( function ( $id ) {
                $user = User::find( $id );

                if ( $user ) {
                    return [ $user->id => $user->email ];
                }
            } )->ajax( '/admin/users/search' );
            $form->select( 'question_id', 'Question' )->options( function ( $id ) {
                $question = Question::find( $id );

                if ( $question ) {
                    return [ $question->id => $question->question ];
                }
            } )->ajax( '/admin/questions/search' );

            $form->display( 'created_at', 'Created At' );
            $form->display( 'updated_at', 'Updated At' );
        } );
    }
}

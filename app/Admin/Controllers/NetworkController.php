<?php

namespace App\Admin\Controllers;

use App\Models\Network;

use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\ModelForm;

class NetworkController extends Controller
{
    use ModelForm;

    /**
     * Index interface.
     *
     * @return Content
     */
    public function index ()
    {
        return Admin::content( function ( Content $content ) {

            $content->header( 'header' );
            $content->description( 'description' );

            $content->body( $this->grid() );
        } );
    }

    /**
     * Edit interface.
     *
     * @param $id
     * @return Content
     */
    public function edit ( $id )
    {
        return Admin::content( function ( Content $content ) use ( $id ) {

            $content->header( 'header' );
            $content->description( 'description' );

            $content->body( $this->form()->edit( $id ) );
        } );
    }

    /**
     * Create interface.
     *
     * @return Content
     */
    public function create ()
    {
        return Admin::content( function ( Content $content ) {

            $content->header( 'header' );
            $content->description( 'description' );

            $content->body( $this->form() );
        } );
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid ()
    {
        return Admin::grid( Network::class, function ( Grid $grid ) {

            $grid->id( 'ID' )->sortable();
            $grid->name( 'Name' )->sortable()->editable();
            $grid->alias( 'Alias' )->sortable();
            $grid->column( 'is_active', 'Status' )->editable( 'select', [
                false => 'Not active',
                true  => 'Active',
            ] );
            $grid->client_id( 'App id' )->editable();
            $grid->client_secret( 'App secret' )->editable();

            $grid->created_at();
            $grid->updated_at();
        } );
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form ()
    {
        return Admin::form( Network::class, function ( Form $form ) {

            $form->display( 'id', 'ID' );
            $form->text( 'name', 'Name' )->rules( 'max:255|required' );
            $form->text( 'alias', 'Alias' )->rules( 'max:255|required' );
            $form->text( 'icon', 'Icon' )->rules( 'max:8|required' );
            $form->text( 'client_id', 'App id' );
            $form->text( 'client_secret', 'App secret' );
            $form->radio( 'is_active', 'Active?' )->options( [
                true  => 'Active',
                false => 'Not active',
            ] );

            $form->display( 'created_at', 'Created At' );
            $form->display( 'updated_at', 'Updated At' );

            $form->saved( function ( $model ) {
                setEnv( strtoupper( $model->alias . '_client_id' ), $model->client_id );
                setEnv( strtoupper( $model->alias . '_client_secret' ), $model->client_secret );
            } );
        } );
    }
}

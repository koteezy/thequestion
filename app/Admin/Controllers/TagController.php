<?php

namespace App\Admin\Controllers;

use App\Models\Tag\Tag;

use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\ModelForm;

class TagController extends Controller
{
    use ModelForm;

    /**
     * Index interface.
     *
     * @return Content
     */
    public function index ()
    {
        return Admin::content( function ( Content $content ) {

            $content->header( 'header' );
            $content->description( 'description' );

            $content->body( $this->grid() );
        } );
    }

    /**
     * Edit interface.
     *
     * @param $id
     * @return Content
     */
    public function edit ( $id )
    {
        return Admin::content( function ( Content $content ) use ( $id ) {

            $content->header( 'header' );
            $content->description( 'description' );

            $content->body( $this->form()->edit( $id ) );
        } );
    }

    /**
     * Create interface.
     *
     * @return Content
     */
    public function create ()
    {
        return Admin::content( function ( Content $content ) {

            $content->header( 'header' );
            $content->description( 'description' );

            $content->body( $this->form() );
        } );
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid ()
    {
        return Admin::grid( Tag::class, function ( Grid $grid ) {

            $grid->id( 'ID' )->sortable();
            $grid->column( 'name', 'Name' )->sortable()
                 ->editable( 'text' );
            $grid->column( 'color', 'Color' )->sortable()
                 ->editable( 'text' );
            $grid->column( 'is_accepted', 'Is accepted?' )->sortable()
                 ->editable( 'select', [ false => 'Declined', true => 'Accepted' ] );

            $grid->created_at();
            $grid->updated_at();
        } );
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form ()
    {
        return Admin::form( Tag::class, function ( Form $form ) {

            $form->display( 'id', 'ID' );
            $form->text( 'name', 'Name' )->rules( 'required|max:50' );
            $form->color( 'color', 'Color' )->rules( 'required|max:15' );
            $form->radio( 'is_accepted', 'Accepted by the moderator?' )
                 ->options( [ 0 => 'Declined', 1 => 'Accepted' ] );

            $form->display( 'created_at', 'Created At' );
            $form->display( 'updated_at', 'Updated At' );
        } );
    }
}

<?php

namespace App;

use App\Models\Network;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name', 'last_name', 'avatar', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $appends = [ 'fullName' ];

    /**
     * @return string
     */
    public function getFullNameAttribute ()
    {
        return $this->first_name . ' ' . $this->last_name;
    }

    /**
     * Get user all synced social networks.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function networks ()
    {
        return $this->belongsToMany( Network::class, 'user_networks', 'user_id', 'network_id' );
    }
}

<?php

namespace App\Services;

use App\Models\Network;
use App\Models\User\UserNetwork;
use App\User;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Laravel\Socialite\Facades\Socialite;
use Laravel\Socialite\Two\FacebookProvider;
use Laravel\Socialite\Facades\Socialite as OldSocialite;

class OauthService
{
    /**
     * @var Network
     */
    protected $network;

    /**
     * What we got from API callback.
     *
     * @var mixed
     */
    protected $user;

    /**
     * OauthBaseService constructor.
     * @param Network $network
     */
    public function __construct ( Network $network )
    {
        $this->network = $network;
    }

    public function redirect ()
    {
        return $this->driver()->redirect();
    }

    public function callback ()
    {
        $this->user = $this->driver()->user();

        return $this->parseUser();
    }

    /**
     * Create new user instance
     *
     * @param array $user
     * @return RedirectResponse|\Illuminate\Routing\Redirector
     */
    protected function createUser ( array $user )
    {
        if ( $this->validatorFails( $user ) )
            return $this->continueRegister( $user );

        // if is a continue register, we need crypt pass.
        if ( isset( $user[ 'password' ] ) )
            $user[ 'password' ] = bcrypt( $user[ 'password' ] );

        // if we can get avatar from social network - get it.
        if ( $this->user->getAvatar() ) {
            $user[ 'avatar' ] = ( new UploadService() )->uploadFromUrl( $this->user->getAvatar() );
        }

        // create user.
        $instance = User::create( $user );

        // Attach network to new user.
        $this->attachNetwork( $instance, $user[ 'uuid' ] );

        return $this->auth( $instance );
    }

    /**
     * @param $uuid
     * @return User|null
     */
    protected function userExists ( $uuid ): ?User
    {
        if ( !auth()->check() )
            return null;

        return User::from( 'users as u' )
                   ->join( 'user_networks as un', 'un.user_id', 'u.id' )
                   ->where( 'un.network_id', $this->network->id )
                   ->where( 'un.uuid', $uuid )
                   ->select( [ 'id', 'email' ] )
                   ->first();
    }

    /**
     * choose what we need do it
     *
     * @param array $user
     * @return RedirectResponse
     */
    protected function makeChoice ( array $user )
    {
        $uuid = $user[ 'uuid' ];

        // if is a continue register with social network.
        if ( $u = unserialize( request()->cookie( 'user' ) ) ) {
            $user = array_merge( $u, [ 'uuid' => $uuid ] );
        }

        if ( auth()->check() ) {
            return $this->attachNetworkToMe( $uuid );
        }

        if ( $this->notAllInformationProvided() ) {
            return $this->continueRegister( $user );
        }

        if ( $instance = $this->userExists( $uuid ) ) {
            return $this->auth( $instance );
        }

        return $this->createUser( $user );
    }

    /**
     * If network already attached to another account.
     *
     * @param $uuid
     * @param bool $attachedToMe
     * @return bool
     */
    private function networkAlreadyAttached ( $uuid, bool $attachedToMe )
    {
        return UserNetwork::whereUuid( $uuid )
                          ->whereNetworkId( $this->network->id )
                          ->where( 'user_id', ( $attachedToMe ? '=' : '!=' ), u()->id )
                          ->exists();
    }

    /**
     * Parse response, get needed fields
     *
     * @return RedirectResponse
     */
    protected function parseUser ()
    {
        $fullName = explode( ' ', $this->user->getName() );

        return $this->makeChoice( [
            'uuid'       => $this->user->getId(),
            'first_name' => $fullName[ 0 ] ?? null,
            'last_name'  => $fullName[ 1 ] ?? null,
            'email'      => $this->user->getEmail(),
        ] );
    }

    /**
     * Check, we have all needed user fields or not?
     *
     * @param array $user
     * @return bool
     */
    private function validatorFails ( array $user )
    {
        $validator = Validator::make( $user, [
            'uuid'       => 'required',
            'first_name' => 'required|max:255',
            'last_name'  => 'required|max:255',
            'email'      => 'required|email|max:255|unique:users,email',
        ] );

        return $validator->fails();
    }

    /**
     * If we do not have enough fields to register,
     * we can return the user to the registration page to enter all the missing fields.
     *
     * @param array $user
     * @return RedirectResponse
     */
    private function continueRegister ( array $user )
    {
        return redirect( route( 'register' ) )
            ->withCookie( 'network_id', $this->network->id )
            ->withInput( $user )
            ->with( 'info', trans( 'messages.networks.continue_register' ) );
    }

    /**
     * @return bool
     */
    private function notAllInformationProvided (): bool
    {
        return !is_null( request()->cookie( 'network_id' ) );
    }

    /**
     * @param string $url
     * @param array $flashes
     * @return RedirectResponse|\Illuminate\Routing\Redirector
     */
    private function redirectResponse ( $url = '/', $flashes = [] )
    {
        Cookie::queue( Cookie::forget( 'network_id' ) );
        Cookie::queue( Cookie::forget( 'user' ) );
        Cookie::queue( Cookie::forget( 'redirect' ) );

        if ( $redirect = request()->cookie( 'redirect' ) ) {
            $url = $redirect;
        }

        return Redirect::to( $url )->with( $flashes );
    }

    /**
     * @param User $user
     * @return RedirectResponse|\Illuminate\Routing\Redirector
     */
    private function auth ( User $user )
    {
        auth()->login( $user );

        return $this->redirectResponse( route( 'index' ) );
    }

    /**
     * @param User $user
     * @param $uuid
     * @return OauthService
     */
    private function attachNetwork ( User $user, $uuid )
    {
        $user->networks()->attach( $this->network->id, [ 'uuid' => $uuid ] );

        return $this;
    }

    /**
     * Just short method.
     *
     * @return mixed
     */
    private function driver ()
    {
        return Socialite::driver( $this->network->alias );
    }

    /**
     * I just decided to bring these conditions here because the chain was getting too big.
     *
     * @param $uuid
     * @return RedirectResponse|\Illuminate\Routing\Redirector
     */
    private function attachNetworkToMe ( $uuid )
    {
        if ( $this->networkAlreadyAttached( $uuid, true ) )
            return $this->redirectResponse( null, [
                'info' => trans( 'messages.networks.network_already_attached_to_my_account' ),
            ] );

        if ( $this->networkAlreadyAttached( $uuid, false ) )
            return $this->redirectResponse( null, [
                'info' => trans( 'messages.networks.already_attached_to_another_account' ),
            ] );

        return $this->attachNetwork( u(), $uuid )->redirectResponse();

    }
}
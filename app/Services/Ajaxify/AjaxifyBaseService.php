<?php

namespace App\Services\Ajaxify;

use Illuminate\Support\Facades\View;

abstract class AjaxifyBaseService
{
    /**
     * @var array
     */
    protected $data = [];

    /**
     * @var array
     */
    protected $css = [];

//    /**
//     * It stores the logic of what information you need to take.
//     * After, this information is placed in $data.
//     *
//     * @return void
//     */
//    protected function setData ()
//    {
//        //
//    }

    /**
     * Is "Loading more" click?
     *
     * @return bool
     */
    protected function isLoadingMore (): bool
    {
        return request()->ajax() && request()->has( 'page' );
    }

    /**
     * @return bool
     */
    protected function isLoadingMoreRaw (): bool
    {
        return request()->ajax() && request()->has( 'page' ) && request()->has( 'mode' );
    }

    /**
     * Is default page view?
     *
     * @return bool
     */
    protected function isHit (): bool
    {
        return !request()->ajax();
    }

    /**
     * Is page view by ajax or default page view?
     *
     * @return bool
     */
    protected function isAjaxOrDefaultHit (): bool
    {
        return !$this->isHit() && !$this->isLoadingMore() || $this->isHit();
    }

    /**
     * Return view.
     *
     * @param string $path
     * @return \Illuminate\Contracts\View\View
     */
    protected function view ( string $path )
    {
        return View::make( $path, array_merge( $this->data, [ 'stylesheets' => $this->css ] ) );
    }

    /**
     * Render html, title
     * for ajax request.
     *
     * @param string $path
     * @return array
     */
    protected function renderHTML ( string $path ): array
    {
        return [
            'content'     => [
                'title' => $this->data[ 'title' ],
                'html'  => $this->view( $path )->render(),
            ],
            'stylesheets' => $this->css,
            'auth'        => auth()->user(),
        ];
    }

}
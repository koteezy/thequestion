<?php

namespace App\Services\Ajaxify;

use App\Repositories\AnswerRepository;
use App\Repositories\QuestionRepository;
use Illuminate\Support\Facades\View;

class AjaxifyQuestionService extends AjaxifyBaseService
{
    /**
     * @var QuestionRepository
     */
    private $questionRepository;
    /**
     * @var AnswerRepository
     */
    private $answerRepository;

    /**
     * AjaxifyFeedService constructor.
     * @param QuestionRepository $questionRepository
     * @param AnswerRepository $answerRepository
     */
    public function __construct ( QuestionRepository $questionRepository, AnswerRepository $answerRepository )
    {
        $this->questionRepository = $questionRepository;
        $this->answerRepository   = $answerRepository;
    }

    public function render ( int $questionId )
    {
        $this->setData( $questionId );

        if ( $this->isHit() )
            return $this->view( 'question' );

        return $this->renderHTML( 'components.question' );
    }


    private function setData ( int $questionId )
    {
        $question = $this->questionRepository->getQuestion( $questionId );

        // If the question is moderated - we need to show it only to the author.
        if ( !$question->was_moderated ) {

            if ( !u() || u()->id !== $question->user_id ) {
                abort( 404 );
            }
        }

        $answers = $this->answerRepository->getQuestionAnswers( $questionId );
        $data    = [];

        $question->view();

        $data[ 'question' ] = $question;
        $data[ 'answers' ]  = $answers;
        $data[ 'title' ]    = $question->question;

        $this->data = $data;
    }

}
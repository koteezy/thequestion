<?php

namespace App\Services\Ajaxify\Tags;

use App\Repositories\QuestionRepository;
use App\Repositories\TagRepository;
use App\Services\Ajaxify\AjaxifyBaseService;

class AjaxifyTagsService extends AjaxifyBaseService
{
    /**
     * @var TagRepository
     */
    private $tagRepository;

    /**
     * @var QuestionRepository
     */
    private $questionRepository;

    /**
     * AjaxifyTagService constructor.
     * @param TagRepository $tagRepository
     */
    public function __construct ( TagRepository $tagRepository )
    {
        $this->tagRepository = $tagRepository;
    }

    public function render ()
    {
        $this->setData();

        if ( $this->isHit() )
            return $this->view( 'tags' );

        if ( $this->isLoadingMore() )
            return $this->view( 'components.tags.list' );

        return $this->renderHTML( 'components.tags' );
    }

    private function setData ()
    {
        $data            = [];
        $data[ 'tags' ]  = $this->tagRepository->getTags()->paginate();
        $data[ 'title' ] = "Список тегов";

        $this->data = $data;
    }
}
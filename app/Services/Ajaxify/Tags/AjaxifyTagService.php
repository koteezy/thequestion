<?php

namespace App\Services\Ajaxify\Tags;

use App\Repositories\QuestionRepository;
use App\Repositories\TagRepository;
use App\Services\Ajaxify\AjaxifyBaseService;

class AjaxifyTagService extends AjaxifyBaseService
{
    /**
     * @var TagRepository
     */
    private $tagRepository;
    /**
     * @var QuestionRepository
     */
    private $questionRepository;

    /**
     * AjaxifyTagService constructor.
     * @param TagRepository $tagRepository
     * @param QuestionRepository $questionRepository
     */
    public function __construct ( TagRepository $tagRepository, QuestionRepository $questionRepository )
    {
        $this->tagRepository      = $tagRepository;
        $this->questionRepository = $questionRepository;
    }

    public function render ( $tagId )
    {
        $this->setData( $tagId );

        if ( $this->isHit() )
            return $this->view( 'tags.tag' );

        if ( $this->isLoadingMore() )
            return $this->view( 'components.questions.list' );

        return $this->renderHTML( 'components.tag' );
    }

    private function setData ( $tagId )
    {
        $data = [];

        if ( $this->isAjaxOrDefaultHit() ) {
            $data[ 'tag' ]   = $this->tagRepository->getTag( $tagId );
            $data[ 'title' ] = $data[ 'tag' ]->name;
        }

        $data[ 'questions' ] = $this->questionRepository->tagQuestions( $tagId )->paginate( 15 );

        $this->data = $data;
    }
}
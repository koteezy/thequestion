<?php

namespace App\Services\Ajaxify\Me;

use App\Repositories\NetworkRepository;
use App\Services\Ajaxify\AjaxifyBaseService;

class AjaxifyProfileSettingsService extends AjaxifyBaseService
{
    protected $css = [
        '/assets/css/bootstrap-social.css',
    ];

    /**
     * @var NetworkRepository
     */
    private $networkRepository;

    /**
     * AjaxifyProfileSettingsService constructor.
     * @param NetworkRepository $networkRepository
     */
    public function __construct ( NetworkRepository $networkRepository )
    {
        $this->networkRepository = $networkRepository;
    }

    public function render ()
    {
        $this->data = [
            'title'    => trans( 'messages.titles.edit_profile' ),
            'networks' => $this->networkRepository->getNetworks(),
        ];

        if ( $this->isHit() )
            return $this->view( 'me.settings' );

        return $this->renderHTML( 'components.me.settings' );
    }
}
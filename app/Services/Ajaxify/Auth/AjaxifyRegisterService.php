<?php

namespace App\Services\Ajaxify\Auth;

use App\Repositories\NetworkRepository;
use App\Services\Ajaxify\AjaxifyBaseService;

class AjaxifyRegisterService extends AjaxifyBaseService
{

    /**
     * @var NetworkRepository
     */
    private $networkRepository;

    protected $css = [
        '/assets/css/bootstrap-social.css',
    ];

    /**
     * AjaxifyLoginService constructor.
     * @param NetworkRepository $networkRepository
     */
    public function __construct ( NetworkRepository $networkRepository )
    {
        $this->networkRepository = $networkRepository;
    }

    public function render ()
    {
        $this->setData();

        if ( $this->isHit() )
            return $this->view( 'auth.register' );

        return $this->renderHTML( 'components.auth.register' );
    }


    private function setData ()
    {
        $data[ 'title' ]    = trans( 'messages.titles.register' );
        $data[ 'networks' ] = $this->networkRepository->getNetworks();

        $this->data = $data;
    }
}
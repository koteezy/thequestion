<?php

namespace App\Services\Ajaxify\Auth;

use App\Repositories\NetworkRepository;
use App\Services\Ajaxify\AjaxifyBaseService;

class AjaxifyLoginService extends AjaxifyBaseService
{
    /**
     * @var NetworkRepository
     */
    private $networkRepository;

    protected $css = [
        '/assets/css/bootstrap-social.css',
    ];

    /**
     * AjaxifyLoginService constructor.
     * @param NetworkRepository $networkRepository
     */
    public function __construct ( NetworkRepository $networkRepository )
    {
        $this->networkRepository = $networkRepository;
    }

    public function render ()
    {
        $this->setData();

        if ( $this->isHit() )
            return $this->view( 'auth.login' );

        return $this->renderHTML( 'components.auth.login' );
    }


    private function setData ()
    {
        $data[ 'title' ]    = trans( 'messages.titles.login' );
        $data[ 'networks' ] = $this->networkRepository->getNetworks();

        $this->data = $data;
    }
}
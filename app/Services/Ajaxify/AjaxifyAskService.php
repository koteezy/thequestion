<?php

namespace App\Services\Ajaxify;

use App\Models\Question\Question;

class AjaxifyAskService extends AjaxifyBaseService
{
    public function render ()
    {
        if ( $this->isHit() )
            return $this->view( 'ask' );

        return $this->renderHTML( 'components.ask' );
    }

    /**
     * Set data.
     *
     * @param int|null $id
     * @return $this
     */
    public function setQuestion ( ?int $id = null )
    {
        $this->data = [
            'title'    => trans( 'messages.titles.' . ( $id ? 'edit_question' : 'create_question' ) ),
            'question' => $id ? Question::whereUserId( u()->id )->findOrFail( $id ) : null,
        ];

        return $this;
    }
}
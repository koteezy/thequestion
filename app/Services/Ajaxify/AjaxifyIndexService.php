<?php

namespace App\Services\Ajaxify;

use App\Repositories\QuestionRepository;
use Illuminate\Support\Facades\View;

class AjaxifyIndexService extends AjaxifyBaseService
{
    /**
     * @var QuestionRepository
     */
    private $questionRepository;

    /**
     * AjaxifyFeedService constructor.
     * @param QuestionRepository $questionRepository
     */
    public function __construct ( QuestionRepository $questionRepository )
    {
        $this->questionRepository = $questionRepository;
    }


    public function render ()
    {
        if ( $this->isHit() )
            return $this->view( 'feed' );
        else if ( $this->isLoadingMoreRaw() ) {
            return $this->renderHTML( 'components.feed' );
        } else if ( $this->isLoadingMore() ) {
            return $this->view( 'components.questions.list' );
        }

        return $this->renderHTML( 'components.feed' );
    }

    /**
     * Get feed questions.
     *
     * @param null $top
     * @return $this
     */
    public function userFeedQuestions ( $top = null )
    {
        $this->data = [
            'questions' => $this->questionRepository->userFeedQuestions( u()->id )->top( $top )->paginate(),
            'title'     => trans( 'messages.titles.my_feed' ),
        ];

        return $this;
    }

    /**
     * Get feed questions.
     *
     * @param null $top
     * @return $this
     */
    public function feedQuestions ( $top = null )
    {
        $this->data = [
            'questions' => $this->questionRepository->feedQuestions()->top( $top )->paginate(),
            'title'     => trans( 'messages.titles.' . ( $top ? 'popular_feed' : 'feed' ) ),
        ];

        return $this;
    }


    /**
     * Get auth user questions.
     *
     * @return $this
     */
    public function myQuestions ()
    {
        $this->data = [
            'questions' => $this->questionRepository->userQuestions( u()->id )->paginate(),
            'title'     => 'My questions',
        ];

        return $this;
    }

    /**
     * @param $top
     * @return array|\Illuminate\Contracts\Translation\Translator|null|string
     */
    private function getTitle ( $top )
    {
        if ( $this->userFeed ) {
            $key = 'my_feed';
        } else if ( $top ) {
            $key = 'popular_feed';
        } else {
            $key = 'feed';
        }

        return trans( 'messages.titles.' . $key );
    }

}
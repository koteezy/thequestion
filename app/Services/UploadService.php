<?php

namespace App\Services;

use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;

class UploadService
{

    /**
     * Upload file.
     *
     * @param UploadedFile $file
     * @return string
     */
    public function upload ( UploadedFile $file ): string
    {
        $md5 = md5( $file->getClientOriginalName() );
        $md5 = substr_replace( $md5, '/', 3, 0 );
        $md5 = substr_replace( $md5, '/', 7, 0 );

        $storagePath = $this->disk()->put( $md5, $file );

        $path = $md5 . '/' . basename( $storagePath );

        return $path;
    }

    /**
     * @param string $url
     * @return bool|string
     */
    public function uploadFromUrl ( string $url )
    {
        try {
            $originalName = md5( str_random( 20 ) ) . '.jpg';
            $info         = pathinfo( $url );
            $contents     = file_get_contents( $url );
            $file         = '/tmp/' . $info[ 'basename' ];
            file_put_contents( $file, $contents );

            $file = new UploadedFile( $file, $originalName );

            return $this->upload( $file );
        } catch ( \Exception $exception ) {

            Log::info( 'Cant load image from url - ' . $exception->getCode() . ' - ' . $exception->getMessage() );

            return null;
        }
    }


    /**
     * @param string $path
     *
     * @return $this
     */
    public function unset ( string $path ): UploadService
    {
        $this->disk()->delete( $path );

        return $this;
    }

    /**
     * @return \Illuminate\Contracts\Filesystem\Filesystem
     */
    private function disk ()
    {
        return Storage::disk( env( 'FILESYSTEM_DRIVER' ) );
    }

}
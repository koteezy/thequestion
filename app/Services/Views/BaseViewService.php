<?php

namespace App\Services\Views;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Cookie;

class BaseViewService
{
    protected $model;
    protected $hash;
    protected $userId;

    /**
     * BaseViewService constructor.
     * @param Model $model
     */
    public function __construct ( Model $model )
    {
        $this->model  = $model;
        $this->hash   = Cookie::get( 'u_hash' );
        $this->userId = auth()->id();
    }
}
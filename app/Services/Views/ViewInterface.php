<?php

namespace App\Services\Views;

interface ViewInterface
{
    public function view ();

    public function isViewed (): bool;
}
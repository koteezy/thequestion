<?php

namespace App\Services\Views;

use App\Models\Question\QuestionView;

class  QuestionViewService extends BaseViewService implements ViewInterface
{
    public function view ()
    {
        if ( $this->isViewed() )
            return;

        QuestionView::create( [
            'question_id' => $this->model->id,
            'user_id'     => $this->userId,
            'hash'        => $this->hash,
            'ip'          => request()->ip(),
        ] );
    }

    public function isViewed (): bool
    {
        return QuestionView::whereHash( $this->hash )
                           ->where( 'question_id', $this->model->id )
                           ->orWhere( 'user_id', $this->userId )
                           ->where( 'question_id', $this->model->id )
                           ->exists();
    }
}
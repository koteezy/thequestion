<?php

namespace App\Services;

use Illuminate\Database\Eloquent\Builder;

class SyncService
{
    /**
     * As user id
     *
     * @var int
     */
    private $as;

    /**
     * Set user
     *
     * @param int $userId
     * @return $this
     */
    public function as ( int $userId )
    {
        $this->as = $userId;

        return $this;
    }

    /**
     * Attach user id to relation, or detach.
     *
     * @param $relation
     * @param array $params
     */
    public function to ( $relation, $params = [] )
    {
        if ( $relation->whereUserId( $this->as )->exists() )
            $relation->detach( $this->as );
        else
            $relation->attach( $this->as, $params );
    }
}
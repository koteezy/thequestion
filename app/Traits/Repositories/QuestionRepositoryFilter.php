<?php

namespace App\Traits\Repositories;

use Illuminate\Database\Eloquent\Builder;

/**
 * @property Builder $model
 *
 * Class QuestionRepositoryFilter
 * @package App\Traits\Repositories
 */
trait QuestionRepositoryFilter
{
    /**
     * @param null $top
     * @return $this
     */
    public function top ( $top = null )
    {
        if ( $top )
            $this->model = $this->model
                ->orderBy( 'views_count', 'desc' )
                ->orderBy( 'answers_count', 'desc' )
                ->orderBy( 'followers_count', 'desc' )
                ->orderBy( 'likes_count', 'desc' );

        return $this;
    }

    /**
     * Get only moderated questions.
     */
    public function moderated ()
    {
        $this->model = $this->model->where( 'q.was_moderated', true );

        return $this;
    }
}